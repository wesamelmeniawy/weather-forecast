package com.wesam.weatherforecast

import android.content.res.Configuration
import android.util.Log
import androidx.multidex.MultiDexApplication
import com.wesam.weatherforecast.utils.getLocale

class MyApplication : MultiDexApplication() {
    override fun onCreate() {
        super.onCreate()
        //setLocale()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        //setLocale()
    }

//    private fun setLocale(){
//        val resources = resources
//        val configuration = resources.configuration
//        val locale = getLocale(this)
//        Log.i("Application","locale ${locale.language}")
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
//            Log.i("Application", "locale ${locale.language}")
//            if(configuration.locales[0] != locale){
//                configuration.setLocale(locale)
//                resources.updateConfiguration(configuration,null)
//            }
//
//        }else{
//            Log.i("Application 26","locale ${locale.language}")
//            if(configuration.locale != locale){
//                configuration.setLocale(locale)
//                resources.updateConfiguration(configuration,null)
//            }
//
//        }
//    }
}