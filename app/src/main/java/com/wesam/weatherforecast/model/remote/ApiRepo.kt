package com.wesam.weatherforecast.model.remote

import com.wesam.weatherforecast.model.data_classes.AlarmTriggers
import com.wesam.weatherforecast.model.data_classes.TriggerRequest
import com.wesam.weatherforecast.model.data_classes.TriggerResponse
import com.wesam.weatherforecast.model.data_classes.Weather
import kotlinx.coroutines.Deferred
import retrofit2.Response

@Suppress("DeferredIsResult")
interface ApiRepo {
     fun getWeather(latitude:Double, longitude:Double, lang:String):Deferred<Response<Weather>>
     fun getWeather(latitude:Float, longitude:Float, lang:String):Deferred<Response<Weather>>
     fun setTrigger(triggerAlarm: TriggerRequest):
             Deferred<Response<TriggerResponse>>

     fun getTrigger(alertTriggerId:String):
             Deferred<Response<AlarmTriggers>>
}