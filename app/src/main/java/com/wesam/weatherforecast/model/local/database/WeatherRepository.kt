package com.wesam.weatherforecast.model.local.database

import android.content.Context
import androidx.lifecycle.LiveData
import com.wesam.weatherforecast.model.data_classes.*

class WeatherRepository(context: Context) :WeatherRepo{
    private var _weatherDao: WeatherDao =
        WeatherDataBase.getDatabase(context.applicationContext).weatherDao()

    override suspend fun getWeather(latitude: Double, longitude: Double): Weather {
        return _weatherDao.getWeather(latitude, longitude)
    }

    override suspend fun getWeather(latitude: Float, longitude: Float): Weather {
        return _weatherDao.getWeather(latitude, longitude)
    }

    override suspend fun getCurrentWeather(cityName: String): Weather = _weatherDao.getWeather(cityName)

    override fun getCurrentWeather(latitude: Float, longitude: Float): Weather {
        return _weatherDao.getCurrentWeather(latitude,longitude)
    }

    override suspend fun deleteRow(latitude: Double, longitude: Double) {
        _weatherDao.deleteRow(latitude, longitude)
    }

    override suspend fun updateWeather(
        current: Current?,
        hourly: List<Hourly?>,
        daily: List<Daily?>,
        alerts: List<Alerts?>,
        latitude: Double,
        longitude: Double
    ) {
        _weatherDao.updateWeather(current,hourly,daily,alerts,latitude, longitude)
    }

    override suspend fun updateWeather(
        current: Current?,
        hourly: List<Hourly?>,
        daily: List<Daily?>,
        alerts: List<Alerts?>,
        latitude: Float,
        longitude: Float
    ) {
        _weatherDao.updateWeather(current,hourly,daily,alerts,latitude, longitude)
    }


    override suspend fun insertCountry(weather:Weather):Long {
       return _weatherDao.insertCountry(weather )
    }

    override  fun getCountryName(latitude: Double, longitude: Double): String {
        return _weatherDao.getCountryName(latitude,longitude)
    }

    override fun getAllWeather(): LiveData<List<Weather>> {
        return  _weatherDao.getAllWeather()
    }
    override fun getAllCountry(): List<String> {
        return _weatherDao.getAllCountry()
    }

    override  fun deleteFavoriteRow(id: Int) {
        _weatherDao.deleteFavoriteRow(id)
    }

    override suspend fun insertAlarm(alarm: Alarm):Long = _weatherDao.insertAlarm(alarm)


    override fun getAllAlarm(): LiveData<List<Alarm>> = _weatherDao.getAllAlarm()

    override fun deleteAlarm(id: Int) {
        _weatherDao.deleteAlarm(id)
    }

    override fun updateAlarmActivation(active: Boolean?, id: Int?) {
        _weatherDao.updateAlarmActivation(active,id)
    }

    override fun updateAlarmStatus(alarm: Boolean?, id: Int?) {
        _weatherDao.updateAlarmStatus(alarm,id)
    }


    override fun updateTriggerId(triggerId: String?,id: Int) {
        _weatherDao.updateTriggerId(triggerId,id)
    }

    override fun getTriggerId(id: Int): Alarm = _weatherDao.getTriggerId(id)
}