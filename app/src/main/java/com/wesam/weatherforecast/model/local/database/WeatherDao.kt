package com.wesam.weatherforecast.model.local.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.wesam.weatherforecast.model.data_classes.*


@Dao
interface WeatherDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
     suspend fun insertCountry(weather:Weather):Long

    @Query(
        " UPDATE weather_table set current = :currentObject," +
                "hourly= :hourlyList, daily= :dailyList," +
                "alerts= :alertsList" +
                " WHERE latitude= :latitude AND longitude= :longitude"
    )
     fun updateWeather(
        currentObject: Current?,
        hourlyList: List<Hourly?>,
        dailyList: List<Daily?>,
        alertsList: List<Alerts?>,
        latitude: Double,
        longitude: Double
    )

    @Query(
        " UPDATE weather_table set current = :currentObject," +
                "hourly= :hourlyList, daily= :dailyList," +
                "alerts= :alertsList" +
                " WHERE latitude= :latitude AND longitude= :longitude"
    )
    fun updateWeather(
        currentObject: Current?,
        hourlyList: List<Hourly?>,
        dailyList: List<Daily?>,
        alertsList: List<Alerts?>,
        latitude: Float,
        longitude: Float
    )

    @Query("DELETE FROM weather_table WHERE latitude= :latitude AND longitude= :longitude ")
     fun  deleteRow(latitude: Double, longitude: Double)

    @Query("SELECT * FROM weather_table WHERE latitude= :latitude AND longitude= :longitude ")
     fun  getWeather(latitude: Double, longitude: Double):Weather

    @Query("SELECT * FROM weather_table WHERE latitude= :latitude AND longitude= :longitude ")
    fun  getWeather(latitude: Float, longitude: Float):Weather

    @Query("SELECT * FROM weather_table WHERE country_name= :cityName")
    fun  getWeather(cityName: String):Weather


    @Query("SELECT * FROM weather_table WHERE latitude= :latitude AND longitude= :longitude ")
    fun  getCurrentWeather(latitude: Float, longitude: Float):Weather

    @Query("SELECT country_name FROM weather_table WHERE latitude= :latitude AND longitude= :longitude ")
    fun  getCountryName(latitude: Double, longitude: Double):String

    @Query("SELECT * FROM weather_table")
    fun getAllWeather():LiveData<List<Weather>>

    @Query("SELECT country_name FROM weather_table ")
    fun  getAllCountry():List<String>

    @Query("DELETE FROM weather_table WHERE weather_id = :id")
    fun  deleteFavoriteRow(id: Int)


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAlarm(alarm:Alarm):Long

    @Query(" UPDATE alarm_table set trigger_id = :triggerId WHERE alarm_id= :id")
    fun updateTriggerId(triggerId: String?,id: Int )

    @Query(" UPDATE alarm_table set active = :active WHERE alarm_id= :id")
    fun updateAlarmActivation(active: Boolean?,id: Int?)

    @Query(" UPDATE alarm_table set alarm = :alarm WHERE alarm_id= :id")
    fun updateAlarmStatus(alarm: Boolean?,id: Int?)


    @Query("DELETE FROM alarm_table WHERE alarm_id = :id")
    fun  deleteAlarm(id: Int)


    @Query("SELECT * FROM alarm_table")
    fun getAllAlarm():LiveData<List<Alarm>>

    @Query("SELECT * FROM alarm_table WHERE alarm_id= :id")
    fun getTriggerId(id:Int):Alarm

}