package com.wesam.weatherforecast.model

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import com.wesam.weatherforecast.model.data_classes.*
import com.wesam.weatherforecast.model.local.database.WeatherRepo
import com.wesam.weatherforecast.model.local.database.WeatherRepository
import com.wesam.weatherforecast.model.local.preferances.MySharedPreference
import com.wesam.weatherforecast.model.local.preferances.PreferenceRepo
import com.wesam.weatherforecast.model.local.preferances.PreferenceRepository
import com.wesam.weatherforecast.model.remote.ApiRepo
import com.wesam.weatherforecast.model.remote.ApiRepository
import com.wesam.weatherforecast.utils.PREF_FILE_NAME
import retrofit2.Response
import java.io.IOException


class ModelRepository(context: Context){
    private val preference = MySharedPreference(context.getSharedPreferences(PREF_FILE_NAME,Context.MODE_PRIVATE))
    private var _apiRepo: ApiRepo = ApiRepository()
    private var _databaseRepo: WeatherRepo = WeatherRepository(context)
    private var _sharedPreference: PreferenceRepo = PreferenceRepository(preference)

    suspend fun getWeather(latitude: Float, longitude: Float,lang:String): Weather {
//
        try {
            val response = _apiRepo.getWeather(latitude, longitude,lang).await()
            if(response.isSuccessful){
                val responseBody = response.body()
                responseBody?.hourly?.let {
                    responseBody.daily.let { it1 ->
                        responseBody.alerts.let { it2 ->
                            _databaseRepo.updateWeather(
                                responseBody.current, it,
                                it1, it2, latitude, longitude) } } }
            }
        }catch (e: IOException){
            Log.e("ModelRepository","IOException ${e.message}")

        }

        return _databaseRepo.getWeather(latitude,longitude)
    }

    suspend fun getWeather(latitude: Float, longitude: Float,lang:String,cityName:String): Weather {

        try {
            val response = _apiRepo.getWeather(latitude, longitude,lang).await()
            if(response.isSuccessful){
                val responseBody = response.body()
                responseBody?.hourly?.let {
                    responseBody.daily.let { it1 ->
                        responseBody.alerts.let { it2 ->
                            _databaseRepo.updateWeather(
                                responseBody.current, it,
                                it1, it2, latitude, longitude) } } }
            }
        }catch (e: IOException){
            Log.e("ModelRepository","IOException ${e.message}")

        }

        return _databaseRepo.getCurrentWeather(cityName)
    }

    suspend fun setTrigger(triggerAlarm: TriggerRequest):Response<TriggerResponse> =
         _apiRepo.setTrigger(triggerAlarm).await()

    suspend fun getTrigger(alertTriggerId:String):Response<AlarmTriggers> =
         _apiRepo.getTrigger(alertTriggerId).await()


//    fun getCurrentWeather(latitude: Float, longitude: Float):Weather{
//        return _databaseRepo.getCurrentWeather(latitude,longitude)
//    }

    fun getTriggerId(id:Int):Alarm = _databaseRepo.getTriggerId(id)

    suspend fun insertCountry(weather: Weather):Long {
        Log.i("insertCountry","insertCountry")
       return _databaseRepo.insertCountry(weather)
    }

//     fun getCountryName(latitude: Double, longitude: Double):String{
//       return _databaseRepo.getCountryName(latitude,longitude)
//    }

    fun getAllWeather():LiveData<List<Weather>>{
        return _databaseRepo.getAllWeather()
    }

    fun getAllCountry():List<String>{
        return _databaseRepo.getAllCountry()
    }

     fun deleteFavoriteRow(id: Int){
        _databaseRepo.deleteFavoriteRow(id)
    }

    fun getPositionedCountryName():String{
        return _sharedPreference.getPositionCountryName()
    }
    fun setPositionedCountryName(countryName: String){
        _sharedPreference.setPositionCountryName(countryName)
    }

    fun getLanguage():String= _sharedPreference.getLanguage()

    fun setLanguage(language: String){
        _sharedPreference.setLanguage(language)
    }

    fun getLocationOption():String=_sharedPreference.getLocationOption()

    fun setLocationOption(locationOption: String){
        _sharedPreference.setLocationOption(locationOption)
    }

    fun getTempUnit():String =_sharedPreference.getTempUnit()

    fun setTempUnit(tempUnit: String){
        _sharedPreference.setTempUnit(tempUnit)
    }

    fun getWindUnit():String= _sharedPreference.getWindUnit()

    fun setWindUnit(windUnit: String){
        _sharedPreference.setWindUnit(windUnit)
    }

     fun getPositionLatitude(): Float = _sharedPreference.getPositionLatitude()

     fun setPositionLatitude(latitude: Float) {
        _sharedPreference.setPositionLatitude(latitude)
    }

     fun getPositionLongitude(): Float = _sharedPreference.getPositionLongitude()

     fun setPositionLongitude(longitude: Float) {
        _sharedPreference.setPositionLongitude(longitude)
    }

    suspend fun insertAlarm(alarm: Alarm):Long =
        _databaseRepo.insertAlarm(alarm)

    fun getAllAlarm():LiveData<List<Alarm>> = _databaseRepo.getAllAlarm()

    fun deleteAlarm(id: Int){
        _databaseRepo.deleteAlarm(id)
    }
    fun updateAlarmActivation(active: Boolean?, id: Int?){
        _databaseRepo.updateAlarmActivation(active, id)
    }
     fun updateTriggerId(triggerId: String?,id: Int){
        _databaseRepo.updateTriggerId(triggerId,id)
    }

    fun updateAlarmStatus(alarm: Boolean?, id: Int?){
        _databaseRepo.updateAlarmStatus(alarm, id)
    }

}