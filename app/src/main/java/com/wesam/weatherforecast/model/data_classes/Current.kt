package com.wesam.weatherforecast.model.data_classes

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Current(
    @Json(name = "dt")
    val date: Long? = 0,

    @Json(name = "temp")
    val temperature: Double? = 0.0,

    @Json(name = "pressure")
    val pressure: Int? = 0,

    @Json(name = "humidity")
    val humidity: Int? = 0,

    @Json(name = "clouds")
    val clouds: Int? = 0,

    @Json(name = "wind_speed")
    val wind_speed: Float? = 0.0f,

    @Json(name = "weather")
    val weather: List<WeatherList?>? = listOf(),


)
