package com.wesam.weatherforecast.model.local.preferances

import com.wesam.weatherforecast.utils.*

class PreferenceRepository(private val _sharedPreference: MySharedPreference) : PreferenceRepo {

    override fun getPositionCountryName(): String = _sharedPreference.getString(KEY_POSITIONED_COUNTRY_NAME)

    override fun setPositionCountryName(countryName: String) {
        _sharedPreference.setString(KEY_POSITIONED_COUNTRY_NAME,countryName)
    }

    override fun getLanguage(): String =  _sharedPreference.getString(KEY_LANG_TYPE)

    override fun setLanguage(language: String) {
        _sharedPreference.setString(KEY_LANG_TYPE,language)
    }

    override fun getTempUnit(): String = _sharedPreference.getString(KEY_TEMP_UNIT)

    override fun setTempUnit(temp: String) {
        _sharedPreference.setString(KEY_TEMP_UNIT,temp)
    }

    override fun getWindUnit(): String = _sharedPreference.getString(KEY_WIND_UNIT)

    override fun setWindUnit(wind: String) {
        _sharedPreference.setString(KEY_WIND_UNIT,wind)
    }

    override fun getLocationOption(): String= _sharedPreference.getString(KEY_LOCATION_OPTION)

    override fun setLocationOption(location: String) {
        _sharedPreference.setString(KEY_LOCATION_OPTION,location)
    }

    override fun getPositionLatitude(): Float = _sharedPreference.getFloat(KEY_POSITION_LATITUDE)

    override fun setPositionLatitude(latitude: Float) {
        _sharedPreference.setFloat(KEY_POSITION_LATITUDE,latitude)
    }

    override fun getPositionLongitude(): Float = _sharedPreference.getFloat(KEY_POSITION_LONGITUDE)

    override fun setPositionLongitude(longitude: Float) {
        _sharedPreference.setFloat(KEY_POSITION_LONGITUDE,longitude)
    }


}