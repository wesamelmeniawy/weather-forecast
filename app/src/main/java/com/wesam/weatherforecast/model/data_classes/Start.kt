package com.wesam.weatherforecast.model.data_classes


import com.squareup.moshi.Json

data class Start(
    @Json(name = "amount")
    val amount: Int? = 0,
    @Json(name = "expression")
    val expression: String? = ""
)