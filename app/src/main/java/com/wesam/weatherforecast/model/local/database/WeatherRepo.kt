package com.wesam.weatherforecast.model.local.database

import androidx.lifecycle.LiveData
import com.wesam.weatherforecast.model.data_classes.*

interface WeatherRepo {
    suspend fun getWeather(latitude: Double, longitude: Double): Weather
    suspend fun getWeather(latitude: Float, longitude: Float): Weather
    suspend fun getCurrentWeather(cityName: String): Weather
    fun getCurrentWeather(latitude: Float, longitude: Float): Weather

    suspend fun deleteRow(latitude: Double, longitude: Double)

    suspend fun updateWeather(
        current: Current?, hourly: List<Hourly?>, daily: List<Daily?>,
        alerts: List<Alerts?>, latitude: Double, longitude: Double
    )

    suspend fun updateWeather(
        current: Current?, hourly: List<Hourly?>, daily: List<Daily?>,
        alerts: List<Alerts?>, latitude: Float, longitude: Float
    )

    suspend fun insertCountry(weather: Weather):Long

    fun getCountryName(latitude: Double, longitude: Double): String

    fun getAllWeather():LiveData<List<Weather>>

    fun getAllCountry():List<String>

    fun deleteFavoriteRow(id: Int)

    suspend fun insertAlarm(alarm: Alarm):Long
    fun getAllAlarm():LiveData<List<Alarm>>

    fun deleteAlarm(id: Int)
    fun updateAlarmActivation(active: Boolean?, id: Int?)
    fun updateAlarmStatus(alarm: Boolean?,id: Int?)
    fun updateTriggerId(triggerId: String?, id: Int)
    fun getTriggerId(id:Int):Alarm


}