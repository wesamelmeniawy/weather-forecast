package com.wesam.weatherforecast.model.data_classes

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
@Entity(tableName = "weather_table")
data class Weather(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "weather_id")
    val id: Int? = null,

    @ColumnInfo(name = "country_name")
    val countryName: String?,

    @ColumnInfo(name = "latitude")
    val latitude: Float?,

    @ColumnInfo(name = "longitude")
    val longitude: Float?,

    @Json(name = "current")
    @ColumnInfo(name = "current")
    val current: Current? = null,

    @Json(name = "hourly")
    @ColumnInfo(name = "hourly")
    val hourly: List<Hourly?> = listOf(),

    @Json(name = "daily")
    @ColumnInfo(name = "daily")
    val daily: List<Daily?> = listOf(),

    @Json(name = "alerts")
    @ColumnInfo(name = "alerts")
    val alerts: List<Alerts?> = listOf(),

    )
