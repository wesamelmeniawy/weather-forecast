package com.wesam.weatherforecast.model.local.database

import androidx.room.TypeConverter
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Types
import com.wesam.weatherforecast.model.data_classes.*
import com.wesam.weatherforecast.utils.moshi

class DatabaseTypeConverter {

    private val moshiObject = moshi

    private val hourlyListType = Types.newParameterizedType(List::class.java, Hourly::class.java)
    private val adapterHourlyList: JsonAdapter<List<Hourly>> = moshiObject.adapter(hourlyListType)

    private val dailyListType = Types.newParameterizedType(List::class.java, Daily::class.java)
    private val adapterDailyList: JsonAdapter<List<Daily>> = moshiObject.adapter(dailyListType)

    private val alertsListType = Types.newParameterizedType(List::class.java, Alerts::class.java)
    private val adapterAlertsList: JsonAdapter<List<Alerts>> = moshiObject.adapter(alertsListType)

    private val weatherListType = Types.newParameterizedType(List::class.java, WeatherList::class.java)
    private val adapterWeatherList: JsonAdapter<List<WeatherList>> = moshiObject.adapter(weatherListType)

    private val adapterCurrent: JsonAdapter<Current?> = moshiObject.adapter(Current::class.java)

    private val adapterTemp: JsonAdapter<Temp> = moshiObject.adapter(Temp::class.java)

    @TypeConverter
    fun stringToCurrentList(value: String): Current? = adapterCurrent.fromJson(value)

    @TypeConverter
    fun stringToTempList(value: String): Temp? = adapterTemp.fromJson(value)

    @TypeConverter
    fun currentToString(current: Current?): String = adapterCurrent.toJson(current)


    @TypeConverter
    fun stringToHourlyList(value: String): List<Hourly> =
        adapterHourlyList.fromJson(value) ?: listOf()

    @TypeConverter
    fun hourlyListToString(list: List<Hourly>): String = adapterHourlyList.toJson(list) ?: ""

    @TypeConverter
    fun stringToWeatherList(value: String): List<WeatherList> =
        adapterWeatherList.fromJson(value) ?: listOf()

    @TypeConverter
    fun weatherListToString(list: List<WeatherList>): String = adapterWeatherList.toJson(list) ?: ""

    @TypeConverter
    fun stringToDailyList(value: String): List<Daily> =
        adapterDailyList.fromJson(value) ?: listOf()

    @TypeConverter
    fun dailyListToString(list: List<Daily>): String = adapterDailyList.toJson(list) ?: ""

    @TypeConverter
    fun stringToAlertsList(value: String): List<Alerts> =
        adapterAlertsList.fromJson(value) ?: listOf()

    @TypeConverter
    fun alertsListToString(list: List<Alerts>): String = adapterAlertsList.toJson(list) ?: ""

}