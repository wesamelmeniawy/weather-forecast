package com.wesam.weatherforecast.model.local.preferances

interface PreferenceRepo {
    fun getPositionCountryName():String
    fun setPositionCountryName(countryName: String)

    fun getLanguage():String
    fun setLanguage(language: String)

    fun getTempUnit():String
    fun setTempUnit(temp: String)

    fun getWindUnit():String
    fun setWindUnit(wind: String)

    fun getLocationOption():String
    fun setLocationOption(location: String)

    fun getPositionLatitude():Float
    fun setPositionLatitude(latitude: Float)

    fun getPositionLongitude():Float
    fun setPositionLongitude(longitude: Float)

}