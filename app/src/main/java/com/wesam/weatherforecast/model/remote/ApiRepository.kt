package com.wesam.weatherforecast.model.remote

import com.wesam.weatherforecast.model.data_classes.AlarmTriggers
import com.wesam.weatherforecast.model.data_classes.TriggerRequest
import com.wesam.weatherforecast.model.data_classes.TriggerResponse
import com.wesam.weatherforecast.model.data_classes.Weather
import kotlinx.coroutines.Deferred
import retrofit2.Response

@Suppress("DeferredIsResult")
class ApiRepository: ApiRepo {

    private  var _apiService: ApiService = NetworkService.createRetrofit().create(ApiService::class.java)

    override  fun getWeather(latitude: Double, longitude: Double,lang:String): Deferred<Response<Weather>> =
        _apiService.getWeather(latitude, longitude,lang)

    override fun getWeather(
        latitude: Float,
        longitude: Float,
        lang: String
    ): Deferred<Response<Weather>> =
        _apiService.getWeather(latitude, longitude,lang)

    override fun setTrigger(triggerAlarm: TriggerRequest): Deferred<Response<TriggerResponse>> =
        _apiService.setTrigger(triggerAlarm)

    override fun getTrigger(alertTriggerId: String): Deferred<Response<AlarmTriggers>> =
        _apiService.getTrigger(alertTriggerId)


}