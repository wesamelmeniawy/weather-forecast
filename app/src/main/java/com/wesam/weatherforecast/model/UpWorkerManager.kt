package com.wesam.weatherforecast.model

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.graphics.Color
import android.media.Ringtone
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.wesam.weatherforecast.R
import com.wesam.weatherforecast.model.data_classes.AlarmTriggers
import com.wesam.weatherforecast.utils.ALARM_ID
import kotlinx.coroutines.*
import retrofit2.Response
import java.io.IOException


class UpWorkerManager(appContext: Context, workerParams: WorkerParameters):
    Worker(appContext, workerParams) {

    private val _modelRepository : ModelRepository = ModelRepository(appContext.applicationContext)
    private var viewModelJob = Job()

    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.IO)

    private lateinit var ringtone:Ringtone

    override fun doWork(): Result {

        sendNotification()
        return Result.success()
    }

    private fun sendNotification(){
        Log.i("sendNotification", "sendNotification")
        val alarmId = inputData.getInt(ALARM_ID, 1)
        Log.i("sendNotification", "sendNotification $alarmId")

        coroutineScope.launch {
            val alarmObject = _modelRepository.getTriggerId(alarmId)
            try {
                if(alarmObject != null){
                    val response:Response<AlarmTriggers>? =alarmObject.triggerId?.let { _modelRepository.getTrigger(it) }
                    if(response?.isSuccessful == true){
                        Log.i("sendNotification", "getTrigger ${response.body()}")
                        Log.i("sendNotification", "getTrigger ${response.body()?.alerts}")
                        if(response.body()?.alerts.toString() != "{}"  ){
                            if((alarmObject.active == true && alarmObject.alarm == true)||
                                (alarmObject.alarm == true && alarmObject.active != true )){
                                alarmObject.eventType?.let { pushNotification(it) }
                                Log.i("sendNotification", "getTrigger")
                                playAlarm()
                            }
                            if(alarmObject.alarm != true && alarmObject.active == true ){
                                alarmObject.eventType?.let { pushNotification(it) }
                            }

                        }
                    }
                    if (alarmObject.repeated == false){
                        _modelRepository.deleteAlarm(alarmId)
                    }
                }
            }catch (e:IOException){
                Log.i("sendNotification", "${e.message}")
            }

        }
    }

    private fun pushNotification(eventType: String){
        var notificationManager: NotificationManager? = null
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            notificationManager = applicationContext.getSystemService(NotificationManager::class.java)
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel("CHANNEL_ID", "name", importance)
            channel.description = "description"
            notificationManager!!.createNotificationChannel(channel)
        }
        val builder = NotificationCompat.Builder(applicationContext, "CHANNEL_ID")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            builder.apply {
                setSmallIcon(R.drawable.logo_icon)
                color = applicationContext.resources.getColor(R.color.white,applicationContext.theme)
                setContentTitle("Weather Forecast")
                setContentText("Alarm for $eventType ")
                priority = NotificationCompat.PRIORITY_DEFAULT
                setAutoCancel(true)
            }
        }else{
            builder.apply {
                setSmallIcon(R.drawable.logo_icon)
                setContentTitle("Weather Forecast")
                setContentText("Alarm for $eventType ")
                priority = NotificationCompat.PRIORITY_DEFAULT
                setAutoCancel(true)
            }
        }

        notificationManager!!.notify(0, builder.build())
    }

    private suspend fun   playAlarm(){
        val sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM)
        ringtone = RingtoneManager.getRingtone(applicationContext, sound)
        ringtone.play()
        delay(5000)
        ringtone.stop()
    }

    override fun onStopped() {
        super.onStopped()
        ringtone.stop()
        viewModelJob.cancel()
    }
}