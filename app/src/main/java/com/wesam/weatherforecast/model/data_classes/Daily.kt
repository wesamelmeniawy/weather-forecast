package com.wesam.weatherforecast.model.data_classes

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Daily(
    @Json(name = "dt")
    val date: Long? = 0,

    @Json(name = "temp")
    val temperature: Temp?,

    @Json(name = "weather")
    val weather: List<WeatherList?> = listOf(),

)
