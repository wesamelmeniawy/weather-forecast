package com.wesam.weatherforecast.model.remote

import com.wesam.weatherforecast.model.data_classes.AlarmTriggers
import com.wesam.weatherforecast.model.data_classes.TriggerRequest
import com.wesam.weatherforecast.model.data_classes.TriggerResponse
import com.wesam.weatherforecast.model.data_classes.Weather
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.*

@Suppress("DeferredIsResult")
interface ApiService {

    @GET("2.5/onecall?exclude=minutely&appid=d210b2c69ac1433046d588237b3c480a")
     fun getWeather(@Query("lat")latitude:Double,@Query("lon")longitude:Double,
                    @Query("lang")lang:String):
            Deferred<Response<Weather>>

    @GET("2.5/onecall?exclude=minutely&appid=d210b2c69ac1433046d588237b3c480a")
    fun getWeather(@Query("lat")latitude:Float,@Query("lon")longitude:Float,
                   @Query("lang")lang:String):
            Deferred<Response<Weather>>


    @POST("3.0/triggers?appid=d210b2c69ac1433046d588237b3c480a")
    fun setTrigger(@Body triggerAlarm:TriggerRequest):
            Deferred<Response<TriggerResponse>>

    @GET("3.0/triggers/{alert}?appid=d210b2c69ac1433046d588237b3c480a")
    fun getTrigger(@Path("alert") alertTriggerId:String):
            Deferred<Response<AlarmTriggers>>
}