package com.wesam.weatherforecast.model.data_classes


import com.squareup.moshi.Json

data class TriggerRequest(
    @Json(name = "area")
    val area: List<Area>? = listOf(),
    @Json(name = "conditions")
    val conditions: List<Condition>? = listOf(),
    @Json(name = "time_period")
    val timePeriod: TimePeriod? = TimePeriod()
)