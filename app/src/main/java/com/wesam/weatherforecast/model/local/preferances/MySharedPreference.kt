package com.wesam.weatherforecast.model.local.preferances

import android.content.SharedPreferences

class MySharedPreference(private val _sharedPreference: SharedPreferences) {

    fun setString(key: String, value: String) {
        _sharedPreference.edit()?.putString(key, value)?.apply()
    }


    fun getString(key: String):String{
       return _sharedPreference.getString(key,"")?:""
    }

    fun setFloat(key: String, value: Float) {
        _sharedPreference.edit()?.putFloat(key, value)?.apply()
    }


    fun getFloat(key: String):Float{
        return _sharedPreference.getFloat(key,0f)
    }

}