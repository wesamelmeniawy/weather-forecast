package com.wesam.weatherforecast.model.data_classes

import com.squareup.moshi.Json

data class TriggerResponse(

    @Json(name = "_id")
    val id :String? = "",

    @Json(name = "alerts")
    val alerts: AlertsTrigger? = null,

    @Json(name = "area")
    val area: List<Area>? = listOf(),
    @Json(name = "conditions")
    val conditions: List<Condition>? = listOf(),
    @Json(name = "time_period")
    val timePeriod: TimePeriod? = TimePeriod()
)
