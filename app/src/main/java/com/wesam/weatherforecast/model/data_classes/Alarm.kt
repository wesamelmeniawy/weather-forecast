package com.wesam.weatherforecast.model.data_classes

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "alarm_table")
data class Alarm(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "alarm_id")
    val id: Int? = null,

    @ColumnInfo(name = "event_type")
    val eventType: String? = "",

    @ColumnInfo(name = "work_manager_tag")
    val workManagerTag: String? = "",

    @ColumnInfo(name = "trigger_id")
    val triggerId: String? = "",

    @ColumnInfo(name = "active")
    val active: Boolean? = false,

    @ColumnInfo(name = "repeated")
    val repeated: Boolean? = false,

    @ColumnInfo(name = "alarm")
    val alarm: Boolean? = false,

    @ColumnInfo(name = "time_from")
    val timeFrom: String? = "",

    @ColumnInfo(name = "time_to")
    val timeTo: String? = "",

    @ColumnInfo(name = "date")
    val date: String? = "",

    @ColumnInfo(name = "notification")
    val notification: Boolean? = false,


)
