package com.wesam.weatherforecast.model.data_classes


import com.squareup.moshi.Json

data class Area(
    @Json(name = "coordinates")
    val coordinates: List<Int>? = listOf(),
    @Json(name = "type")
    val type: String? = ""
)