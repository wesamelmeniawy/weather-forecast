package com.wesam.weatherforecast.model.data_classes

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass


@JsonClass(generateAdapter = true)
data class WeatherList(

    @Json(name = "main")
    val main: String? = "",

    @Json(name = "description")
    val description: String? = "",

    @Json(name = "icon")
    val icon: String? = ""

)
