package com.wesam.weatherforecast.model.data_classes

import com.squareup.moshi.Json

data class AlarmTriggers(
    @Json(name = "_id")
    val id :String? = "",

    @Json(name = "alerts")
    val alerts :Any,
)
