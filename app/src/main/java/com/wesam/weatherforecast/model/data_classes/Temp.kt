package com.wesam.weatherforecast.model.data_classes

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass


@JsonClass(generateAdapter = true)
data class Temp(
    @Json(name = "day")
    val dayTemperature: Double?,

    @Json(name = "min")
    val minTemperature: Double?,

    @Json(name = "max")
    val maxTemperature: Double?,
)
