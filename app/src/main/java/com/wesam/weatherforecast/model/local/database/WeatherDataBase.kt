package com.wesam.weatherforecast.model.local.database

import android.content.Context
import androidx.room.*
import com.wesam.weatherforecast.model.data_classes.Alarm
import com.wesam.weatherforecast.model.data_classes.Weather


@Database(entities = [Weather::class,Alarm::class], version = 1, exportSchema = false)
@TypeConverters(DatabaseTypeConverter::class)
abstract class WeatherDataBase : RoomDatabase(){
    abstract fun weatherDao():WeatherDao

    companion object {
        private var INSTANCE: WeatherDataBase? = null

        fun getDatabase(
            context: Context
        ): WeatherDataBase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    WeatherDataBase::class.java,
                    "weather_database"
                )
                    .fallbackToDestructiveMigration()
                    .allowMainThreadQueries()
                    //.addCallback(WeatherDataBase(scope))
                    .build()
                INSTANCE = instance
                instance
            }
        }
    }
}