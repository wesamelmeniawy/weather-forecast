package com.wesam.weatherforecast.model.data_classes

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Alerts(
    @Json(name = "event")
    val event: String? = "",

    @Json(name = "start")
    val start: Long?,

    @Json(name = "end")
    val end: Long?,

    @Json(name = "description")
    val description: String? = ""
)
