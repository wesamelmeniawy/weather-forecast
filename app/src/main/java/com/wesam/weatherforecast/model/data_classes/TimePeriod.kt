package com.wesam.weatherforecast.model.data_classes


import com.squareup.moshi.Json

data class TimePeriod(
    @Json(name = "end")
    val end: End? = End(),
    @Json(name = "start")
    val start: Start? = Start()
)