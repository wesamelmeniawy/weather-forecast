package com.wesam.weatherforecast.ui.addalarm


import android.app.DatePickerDialog
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.ViewModelProvider
import com.wesam.weatherforecast.databinding.ActivityAddAlarmBinding
import com.wesam.weatherforecast.ui.BaseActivity
import com.wesam.weatherforecast.ui.alarm.AddAlarmViewModel
import java.util.*

class AddAlarmActivity : BaseActivity(){

    private lateinit var binding: ActivityAddAlarmBinding
    private lateinit var viewModel: AddAlarmViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityAddAlarmBinding.inflate(layoutInflater)

        viewModel = ViewModelProvider(
            this, ViewModelProvider.AndroidViewModelFactory(application)
        ).get(AddAlarmViewModel::class.java)


        init()

        setContentView(binding.root)
    }

    private fun init(){
        binding.viewModel = viewModel

        observeData()
        buttonBackClicked()

    }
    private fun observeData(){
        observeButtonAddAlarm()
        observeAddDate()
        observeAddTimeFrom()
        observeAddTimeTo()
    }

    private fun observeButtonAddAlarm(){
        viewModel.addAlarm.observe(this,{
            it.getContentIfNotHandled()?.let {
                finish()
            }
        })
    }
    private fun observeAddDate(){
        viewModel.addDate.observe(this, {
            it.getContentIfNotHandled()?.let {it1->
                Log.i(TAG,"observeAddDate $it1")
            }
        })

    }
    private fun observeAddTimeFrom(){
        viewModel.addTimeFrom.observe(this,{
            it.getContentIfNotHandled()?.let {it1->
                Log.i(TAG,"observeAddTimeFrom $it1")
            }
        })
    }
    private fun observeAddTimeTo(){
        viewModel.addTimeTo.observe(this,{
            it.getContentIfNotHandled()?.let {it1->
                Log.i(TAG,"observeAddTimeTo $it1")
            }
        })

    }
    private fun buttonBackClicked(){
        binding.layoutToolbarBinding.imgBack.setOnClickListener {
            finish()
        }
    }

    companion object{
        const val TAG = "AddAlarmActivity"
    }
}