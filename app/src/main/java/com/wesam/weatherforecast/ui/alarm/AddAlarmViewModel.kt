package com.wesam.weatherforecast.ui.alarm

import android.app.Application
import android.util.Log
import android.view.View
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.work.*
import com.wesam.weatherforecast.model.ModelRepository
import com.wesam.weatherforecast.model.UpWorkerManager
import com.wesam.weatherforecast.model.data_classes.*
import com.wesam.weatherforecast.utils.ALARM_ID
import com.wesam.weatherforecast.utils.Event
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.IOException
import java.util.concurrent.TimeUnit


class AddAlarmViewModel(application: Application): AndroidViewModel(application) {

    private val _modelRepository : ModelRepository = ModelRepository(application.applicationContext)
    private val alarmWorkManager: WorkManager =  WorkManager.getInstance(application)
    private val data: Data.Builder = Data.Builder()

    private var _addAlarm = MutableLiveData<Event<Boolean>>()
    private var _addDate = MutableLiveData<Event<Boolean>>()
    private var _addTimeFrom = MutableLiveData<Event<Boolean>>()
    private var _addTimeTo = MutableLiveData<Event<Boolean>>()
    private var _cancel = MutableLiveData<Event<Boolean>>()
    private var _addTriggerSuccessfully = MutableLiveData<Event<Boolean>>()
    private var _triggerAddedFailed = MutableLiveData<Event<String>>()

    private var _cancelButtonVisibility = MutableLiveData<Int>()
    private var _addButtonVisibility = MutableLiveData<Int>()
    private var _progressVisibility = MutableLiveData<Int>()


    private var _dateString = MutableLiveData<String>()
    private var _timeFromString = MutableLiveData<String>()
    private var _timeToString = MutableLiveData<String>()


    val addAlarm: LiveData<Event<Boolean>>
    get() = _addAlarm

    val addDate: LiveData<Event<Boolean>>
        get() = _addDate

    val addTimeFrom: LiveData<Event<Boolean>>
        get() = _addTimeFrom

    val addTimeTo: LiveData<Event<Boolean>>
        get() = _addTimeTo

    val cancel: LiveData<Event<Boolean>>
        get() = _cancel

    val dateString: LiveData<String>
        get() = _dateString

    val timeFromString: LiveData<String>
        get() = _timeFromString

    val timeToString: LiveData<String>
        get() = _timeToString

    val addTriggerSuccessfully: LiveData<Event<Boolean>>
        get() = _addTriggerSuccessfully

    val triggerAddedFailed: LiveData<Event<String>>
        get() = _triggerAddedFailed

    val addButtonVisibility: LiveData<Int>
        get() = _addButtonVisibility

    val cancelButtonVisibility: LiveData<Int>
        get() = _cancelButtonVisibility

    val progressVisibility: LiveData<Int>
        get() = _progressVisibility



    init {
        setItemVisibility()
    }

    fun addAlarmClicked(){
        _addAlarm.postValue(Event(true))
    }
    fun addDateClicked(){
        _addDate.postValue(Event(true))
    }
    fun addTimeFromClicked(){
        _addTimeFrom.postValue(Event(true))
    }
    fun addTimeToClicked(){
        _addTimeTo.postValue(Event(true))
    }
    fun cancelButtonClicked(){
        _cancel.postValue(Event(true))
    }

    fun saveAlarmData(alarm: Alarm,triggerRequest:TriggerRequest,
                      repeated:Boolean,durationTime:Long,tag:String) {
        _progressVisibility.postValue(View.VISIBLE)
        _addButtonVisibility.postValue(View.GONE)
        _cancelButtonVisibility.postValue(View.GONE)
        viewModelScope.launch(Dispatchers.IO) {

            try {
                val response = _modelRepository.setTrigger(triggerRequest)
                if(response.isSuccessful){
                    val responseBody = response.body()
                    Log.e("ModelRepository","responseBody $responseBody")
                    val id = _modelRepository.insertAlarm(alarm)
                    setWorkManager(repeated,durationTime,tag,id.toInt())
                    _modelRepository.updateTriggerId(responseBody?.id, id.toInt())
                    _addTriggerSuccessfully.postValue(Event(true))
                    setItemVisibility()
                }else{
                    _triggerAddedFailed.postValue(
                        Event("Fail to add alarm please try in another time"))
                    setItemVisibility()
                }
            }catch (e: IOException){
                Log.e("ModelRepository","IOException ${e.message}")
                _triggerAddedFailed.postValue(
                    Event("Fail to add alarm please try in another time"))
                setItemVisibility()
            }
        }
    }
    private fun setWorkManager(repeated:Boolean,durationTime:Long,tag:String,alarmId:Int){
        if(repeated){
            addAlarmWorkRepeated(durationTime, tag,alarmId)
        }else{
            addAlarmOneTime(durationTime,tag,alarmId)
        }

    }
    private fun addAlarmOneTime(
        duration: Long,
        tag: String,
        alarmId: Int
    ) {
        data.putInt(ALARM_ID, alarmId)
        val uploadWorkRequest: WorkRequest = OneTimeWorkRequest.Builder(UpWorkerManager::class.java)
            .setInitialDelay(duration, TimeUnit.MILLISECONDS)
            .addTag(tag)
            .setInputData(data.build())
            .build()
        alarmWorkManager.enqueue(uploadWorkRequest)
        Log.i("addAlarmOneTime","addAlarmOneTime")
    }

    private fun addAlarmWorkRepeated(
        duration: Long,
        tag: String,
        alarmId: Int
    ) {
        data.putInt(ALARM_ID, alarmId)
        val uploadWorkRequest: WorkRequest = PeriodicWorkRequest.Builder(
            UpWorkerManager::class.java,1,TimeUnit.DAYS)
            .setInitialDelay(duration,TimeUnit.MILLISECONDS)
            .addTag(tag)
            .setInputData(data.build())
            .build()
        alarmWorkManager.enqueue(uploadWorkRequest)
        Log.i("addAlarmWorkRepeated","addAlarmWorkRepeated")
    }
    private fun setItemVisibility(){
        _progressVisibility.postValue(View.GONE)
        _addButtonVisibility.postValue(View.VISIBLE)
        _cancelButtonVisibility.postValue(View.VISIBLE)
    }
}