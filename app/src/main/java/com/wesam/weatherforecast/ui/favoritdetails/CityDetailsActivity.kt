package com.wesam.weatherforecast.ui.favoritdetails

import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import com.wesam.weatherforecast.databinding.ActivityCityDetailsBinding
import com.wesam.weatherforecast.ui.BaseActivity
import com.wesam.weatherforecast.utils.KEY_DETAILS_STRING

class CityDetailsActivity : BaseActivity(){

    private lateinit var binding: ActivityCityDetailsBinding
    private lateinit var viewModel: FavoriteDetailsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityCityDetailsBinding.inflate(layoutInflater)

        viewModel = ViewModelProvider(
            this, ViewModelProvider.AndroidViewModelFactory(application)
        ).get(FavoriteDetailsViewModel::class.java)

        init()

        setContentView(binding.root)
    }

    private fun init(){
        binding.viewModel = viewModel
        val weatherString = intent.getStringExtra(KEY_DETAILS_STRING)
        weatherString?.let { viewModel.getList(it) }
        buttonBackClicked()
    }

    private fun buttonBackClicked(){
        binding.layoutToolbarBinding.imgBack.setOnClickListener{
            finish()
        }
    }
}