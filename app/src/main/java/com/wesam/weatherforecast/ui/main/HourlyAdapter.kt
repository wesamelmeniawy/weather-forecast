package com.wesam.weatherforecast.ui.main

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.wesam.weatherforecast.databinding.HourlyRowItemBinding
import com.wesam.weatherforecast.model.data_classes.Hourly
import com.wesam.weatherforecast.utils.*
import com.wesam.weatherforecast.utils.helper.calTempUnit
import com.wesam.weatherforecast.utils.helper.dateFormat
import com.wesam.weatherforecast.utils.helper.splitDate
import java.util.*
import kotlin.collections.ArrayList

class HourlyAdapter(private var tempUnit: String, private var langType: String): RecyclerView.Adapter<HourlyAdapter.ViewHolder>(){

    private var hourlyList: List<Hourly?> = ArrayList()
    private var list: List<String> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val viewBinding = HourlyRowItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent, false
        )
        return ViewHolder(viewBinding)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        list = if (langType == DEFAULT_LANG_STRING){
            splitDate(dateFormat(hourlyList[position]?.date, Locale.ENGLISH), Locale.ENGLISH)

        }else{
            //splitDate(dateFormat(hourlyList[position]?.date, Locale("ar")), Locale("ar"))
            splitDate(dateFormat(hourlyList[position]?.date, Locale("ar")),Locale.ENGLISH)
        }

        if (list.size > 2){
            holder.binding.textTime.text = list[3]
        }

        //holder.binding.textTemp.text = hourlyList[position]?.temperature?.toInt().toString()

        holder.binding.textTemp.text = calTempUnit(hourlyList[position]?.
        temperature,tempUnit)

        val url = IMG_URL + hourlyList[position]?.weather?.get(0)!!.icon+".png"

        Glide.with(holder.binding.imgWeather.context)
            .load(url)
            .into(holder.binding.imgWeather)
    }
    inner class ViewHolder(var binding: HourlyRowItemBinding) :
        RecyclerView.ViewHolder(binding.root)


    override fun getItemCount(): Int {
        return hourlyList.size
    }

    fun loadData(hourlyList: List<Hourly?>, tempUnit: String, langType: String) {
        this.langType = langType
        this.tempUnit = tempUnit
        this.hourlyList = hourlyList
        notifyDataSetChanged()
    }

}