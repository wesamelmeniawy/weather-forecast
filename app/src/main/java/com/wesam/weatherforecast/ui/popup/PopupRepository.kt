package com.wesam.weatherforecast.ui.popup

import android.app.Application
import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.widget.LinearLayout
import android.widget.PopupWindow
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.wesam.weatherforecast.R
import com.wesam.weatherforecast.databinding.PopupTakeRequestBinding
import com.wesam.weatherforecast.databinding.PopupTempWindowBinding
import com.wesam.weatherforecast.databinding.PopupWindowBinding
import com.wesam.weatherforecast.model.ModelRepository
import com.wesam.weatherforecast.ui.alarm.AlarmViewModel
import com.wesam.weatherforecast.ui.favorite.FavoriteViewModel
import com.wesam.weatherforecast.ui.setting.SettingActivity
import com.wesam.weatherforecast.ui.setting.SettingViewModel
import com.wesam.weatherforecast.utils.*
import com.wesam.weatherforecast.utils.helper.navigateActivity


class PopupRepository(application: Application) : PopupRepo {

    private val modelRepository:ModelRepository = ModelRepository(application.applicationContext)

    override fun openPopupTempWindow( layoutInflater: LayoutInflater,
        viewModel: SettingViewModel, context: Context
    ):PopupWindow {
        return openPopupTemp(layoutInflater, viewModel,context)
    }

    override fun openPopupLangWindow(
        layoutInflater: LayoutInflater,
        viewModel: SettingViewModel,
        activity: SettingActivity,
        context: Context
    ):PopupWindow{
        return openPopupLang(layoutInflater, viewModel, activity,context)
    }

    override fun openPopupLocationWindow(
        layoutInflater: LayoutInflater,
        viewModel: SettingViewModel,
        context: Context
    ):PopupWindow {
        return openPopupLocation(layoutInflater, viewModel, context)
    }

    override fun openPopupWindWindow(
        layoutInflater: LayoutInflater,
        viewModel: SettingViewModel,
        context: Context
    ):PopupWindow {
        return openPopupWind(layoutInflater, viewModel, context)
    }

    override fun openRequestPopupWindow(
        layoutInflater: LayoutInflater,
        viewModel: FavoriteViewModel,
        title: String,
        message: String,
        buttonText: String,
        id: Int,
        context: Context,
        currentCityName: String,
        positionedCityName: String
    ):PopupWindow{
        return openPopupRequest(layoutInflater, viewModel, title, message, buttonText, id, context,
        currentCityName, positionedCityName)
    }

    override fun openDeletePopupWindow(
        layoutInflater: LayoutInflater,
        viewModel: AlarmViewModel,
        title: String,
        message: String,
        buttonText: String,
        id: Int,
        tag: String,
    ): PopupWindow {
        return openPopupDelete(layoutInflater, viewModel, title, message, buttonText, id,tag)
    }

    private fun openPopupTemp(
        layoutInflater: LayoutInflater,
        viewModel: SettingViewModel,
        context: Context
    ):PopupWindow{
        val binding = PopupTempWindowBinding.inflate(layoutInflater)
        val popupView = binding.root
        val width = LinearLayout.LayoutParams.MATCH_PARENT
        val height  = LinearLayout.LayoutParams.WRAP_CONTENT
        val focusable = true
        val popupWindow = PopupWindow(popupView, width, height, focusable)
        popupWindow.showAtLocation(popupView, Gravity.BOTTOM, 0, 0)

        when(modelRepository.getTempUnit()){
            DEFAULT_TEMP_STRING -> {
                binding.firstItem.imgRadioCheck.setImageResource(R.drawable.ic_radio_button_checked)
            }
            CELSIUS_TEMP_STRING -> {
                binding.secondItem.imgRadioCheck.setImageResource(R.drawable.ic_radio_button_checked)
            }
            FAHRENHEIT_TEMP_STRING -> {
                binding.thirdItem.imgRadioCheck.setImageResource(R.drawable.ic_radio_button_checked)
            }
        }

        tempWindowButtonsClicked(binding, popupWindow, viewModel,context)
        return popupWindow
    }

    private fun tempWindowButtonsClicked(
        binding: PopupTempWindowBinding, popupWindow: PopupWindow,
        viewModel: SettingViewModel,
        context: Context
    ){

        binding.cancelButton.setOnClickListener{
            popupWindow.dismiss()
        }

        binding.firstItem.textUnit.setOnClickListener{
            binding.firstItem.imgRadioCheck.setImageResource(R.drawable.ic_radio_button_checked)
            binding.secondItem.imgRadioCheck.setImageResource(R.drawable.ic_radio_button_unchecked)
            binding.thirdItem.imgRadioCheck.setImageResource(R.drawable.ic_radio_button_unchecked)

            modelRepository.setTempUnit(DEFAULT_TEMP_STRING)
            viewModel.tempString()
            sendBroadCast(context,KEY_TEMP_UNIT_BROADCAST)
            popupWindow.dismiss()
        }
        binding.secondItem.textUnit.setOnClickListener{
            binding.secondItem.imgRadioCheck.setImageResource(R.drawable.ic_radio_button_checked)
            binding.thirdItem.imgRadioCheck.setImageResource(R.drawable.ic_radio_button_unchecked)
            binding.firstItem.imgRadioCheck.setImageResource(R.drawable.ic_radio_button_unchecked)

            modelRepository.setTempUnit(CELSIUS_TEMP_STRING)
            Log.i("TempItemActivity", modelRepository.getTempUnit())
            viewModel.tempString()
            sendBroadCast(context,KEY_TEMP_UNIT_BROADCAST)
            popupWindow.dismiss()
        }
        binding.thirdItem.textUnit.setOnClickListener{
            binding.thirdItem.imgRadioCheck.setImageResource(R.drawable.ic_radio_button_checked)
            binding.firstItem.imgRadioCheck.setImageResource(R.drawable.ic_radio_button_unchecked)
            binding.secondItem.imgRadioCheck.setImageResource(R.drawable.ic_radio_button_unchecked)
            modelRepository.setTempUnit(FAHRENHEIT_TEMP_STRING)
            viewModel.tempString()
            sendBroadCast(context,KEY_TEMP_UNIT_BROADCAST)
            popupWindow.dismiss()
        }
    }
    private fun openPopupLang(
        layoutInflater: LayoutInflater,
        viewModel: SettingViewModel,
        activity: SettingActivity,
        context: Context
    ):PopupWindow{
        val binding = PopupWindowBinding.inflate(layoutInflater)
        val popupView = binding.root
        val width = LinearLayout.LayoutParams.MATCH_PARENT
        val height  = LinearLayout.LayoutParams.WRAP_CONTENT
        val focusable = true
        val popupWindow = PopupWindow(popupView, width, height, focusable)
        popupWindow.showAtLocation(popupView, Gravity.BOTTOM, 0, 0)


        when(modelRepository.getLanguage()){
            DEFAULT_LANG_STRING -> {
                binding.firstItem.imgRadioCheck.setImageResource(R.drawable.ic_radio_button_checked)
            }
            ARABIC_LANG_STRING -> {
                binding.secondItem.imgRadioCheck.setImageResource(R.drawable.ic_radio_button_checked)
            }
        }

        binding.textTitle.text = popupView.resources.getString(R.string.language)
        binding.firstItem.itemName = popupView.resources.getString(R.string.english)
        binding.secondItem.itemName = popupView.resources.getString(R.string.arabic)

        langWindowButtonsClicked(binding, popupWindow, viewModel, activity, context)
        return popupWindow
    }

    private fun langWindowButtonsClicked(
        binding: PopupWindowBinding,
        popupWindow: PopupWindow,
        viewModel: SettingViewModel,
        activity: SettingActivity,
        context: Context
    ){

        binding.cancelButton.setOnClickListener{
            popupWindow.dismiss()
        }


        binding.firstItem.textUnit.setOnClickListener{
            binding.firstItem.imgRadioCheck.setImageResource(R.drawable.ic_radio_button_checked)
            binding.secondItem.imgRadioCheck.setImageResource(R.drawable.ic_radio_button_unchecked)
            modelRepository.setLanguage(DEFAULT_LANG_STRING)
            viewModel.langString()
            sendBroadCast(context,CURRENT_LANGUAGE)
            activity.finish()
            navigateActivity(activity, SettingActivity())
            popupWindow.dismiss()
        }
        binding.secondItem.textUnit.setOnClickListener{
            binding.secondItem.imgRadioCheck.setImageResource(R.drawable.ic_radio_button_checked)
            binding.firstItem.imgRadioCheck.setImageResource(R.drawable.ic_radio_button_unchecked)
            modelRepository.setLanguage(ARABIC_LANG_STRING)
            viewModel.langString()
            sendBroadCast(activity.applicationContext,CURRENT_LANGUAGE)
            activity.finish()
            navigateActivity(activity, SettingActivity())
            popupWindow.dismiss()
        }
    }
    private fun openPopupLocation(
        layoutInflater: LayoutInflater,
        viewModel: SettingViewModel, context: Context
    ):PopupWindow{
        val binding = PopupWindowBinding.inflate(layoutInflater)
        val popupView = binding.root
        val width = LinearLayout.LayoutParams.MATCH_PARENT
        val height  = LinearLayout.LayoutParams.WRAP_CONTENT
        val focusable = true
        val popupWindow = PopupWindow(popupView, width, height, focusable)
        popupWindow.showAtLocation(popupView, Gravity.BOTTOM, 0, 0)


        when(modelRepository.getLocationOption()){
            DEFAULT_LOCATION_STRING -> {
                binding.firstItem.imgRadioCheck.setImageResource(R.drawable.ic_radio_button_checked)
            }
            MAP_LOCATION_STRING -> {
                binding.secondItem.imgRadioCheck.setImageResource(R.drawable.ic_radio_button_checked)
            }
        }

        binding.textTitle.text = popupView.resources.getString(R.string.location)
        binding.firstItem.itemName = popupView.resources.getString(R.string.popup_gps)
        binding.secondItem.itemName = popupView.resources.getString(R.string.popup_map)

        locationWindowButtonsClicked(binding, popupWindow, viewModel, context)
        return popupWindow
    }

    private fun locationWindowButtonsClicked(
        binding: PopupWindowBinding, popupWindow: PopupWindow, viewModel: SettingViewModel,
        context: Context
    ){

        binding.cancelButton.setOnClickListener{
            popupWindow.dismiss()
        }

        binding.firstItem.textUnit.setOnClickListener{
            binding.firstItem.imgRadioCheck.setImageResource(R.drawable.ic_radio_button_checked)
            binding.secondItem.imgRadioCheck.setImageResource(R.drawable.ic_radio_button_unchecked)
            modelRepository.setLocationOption(DEFAULT_LOCATION_STRING)
            viewModel.locationString()
            sendBroadCast(context, KEY_CHANGE_LOCATION_BROADCAST)
            popupWindow.dismiss()
        }
        binding.secondItem.textUnit.setOnClickListener{
            binding.secondItem.imgRadioCheck.setImageResource(R.drawable.ic_radio_button_checked)
            binding.firstItem.imgRadioCheck.setImageResource(R.drawable.ic_radio_button_unchecked)
            modelRepository.setLocationOption(MAP_LOCATION_STRING)
            viewModel.locationString()
            sendBroadCast(context, KEY_CHANGE_LOCATION_BROADCAST)
            popupWindow.dismiss()

        }

    }
    private fun openPopupWind(
       layoutInflater: LayoutInflater,
        viewModel: SettingViewModel,
       context: Context
    ):PopupWindow{
        val binding = PopupWindowBinding.inflate(layoutInflater)
        val popupView = binding.root
        val width = LinearLayout.LayoutParams.MATCH_PARENT
        val height  = LinearLayout.LayoutParams.WRAP_CONTENT
        val focusable = true
        val popupWindow = PopupWindow(popupView, width, height, focusable)
        popupWindow.showAtLocation(popupView, Gravity.BOTTOM, 0, 0)

        when(modelRepository.getWindUnit()){
            DEFAULT_WIND_STRING -> {
                binding.firstItem.imgRadioCheck.setImageResource(R.drawable.ic_radio_button_checked)
            }
            HOUR_WIND_STRING -> {
                binding.secondItem.imgRadioCheck.setImageResource(R.drawable.ic_radio_button_checked)
            }
        }
        binding.textTitle.text = popupView.resources.getString(R.string.wind_speed_unit)
        binding.firstItem.itemName = popupView.resources.getString(R.string.miles_sec)
        binding.secondItem.itemName = popupView.resources.getString(R.string.miles_hour)

        windWindowButtonsClicked(binding, popupWindow, viewModel, context)
        return popupWindow
    }

    private fun windWindowButtonsClicked(
        binding: PopupWindowBinding, popupWindow: PopupWindow,
        viewModel: SettingViewModel,
        context: Context
    ){

        binding.cancelButton.setOnClickListener{
            popupWindow.dismiss()
        }

        binding.firstItem.textUnit.setOnClickListener{
            binding.firstItem.imgRadioCheck.setImageResource(R.drawable.ic_radio_button_checked)
            binding.secondItem.imgRadioCheck.setImageResource(R.drawable.ic_radio_button_unchecked)
            modelRepository.setWindUnit(DEFAULT_WIND_STRING)
            viewModel.windString()
            sendBroadCast(context, KEY_WIND_UNIT_BROADCAST)
            popupWindow.dismiss()
        }
        binding.secondItem.textUnit.setOnClickListener{
            binding.secondItem.imgRadioCheck.setImageResource(R.drawable.ic_radio_button_checked)
            binding.firstItem.imgRadioCheck.setImageResource(R.drawable.ic_radio_button_unchecked)
            modelRepository.setWindUnit(HOUR_WIND_STRING)
            viewModel.windString()
            sendBroadCast(context, KEY_WIND_UNIT_BROADCAST)
            popupWindow.dismiss()
        }

    }
    private fun openPopupRequest(
        layoutInflater: LayoutInflater,
        viewModel: FavoriteViewModel,
        title: String,
        message: String,
        buttonText:String,
        id: Int,
        context: Context,
        currentCityName: String,
        positionedCityName: String
    ):PopupWindow {
        val binding = PopupTakeRequestBinding.inflate(layoutInflater)
        val popupView = binding.root
        val width = LinearLayout.LayoutParams.MATCH_PARENT
        val height = LinearLayout.LayoutParams.WRAP_CONTENT
        val focusable = false
        val popupWindow = PopupWindow(popupView, width, height, focusable)
        popupWindow.showAtLocation(popupView, Gravity.BOTTOM, 0, 0)
        //popupWindow.isOutsideTouchable = false
        //langWindowButtonsClicked(binding, popupWindow, viewModel, activity)
        binding.textTitle.text = title
        binding.textMessage.text = message
        binding.buttonOk.text = buttonText
        requestWindowButtonsClicked(binding, popupWindow, viewModel, id, context,
        currentCityName, positionedCityName)
        return popupWindow
    }
    private fun requestWindowButtonsClicked(
        binding: PopupTakeRequestBinding, popupWindow: PopupWindow,
        viewModel: FavoriteViewModel,id: Int, context: Context,currentCityName: String,
        positionedCityName: String
    ){

        binding.buttonCancel.setOnClickListener{
            sendBroadCast(context, DELETE_ITEM_CANCEL)
            popupWindow.dismiss()
        }

        binding.buttonOk.setOnClickListener{
            viewModel.deleteRow(id)
            if(currentCityName == positionedCityName){
                modelRepository.setPositionedCountryName("")
                sendBroadCast(context, KEY_CURRENT_POSITION_FAVORITE_BROADCAST)
            }
            popupWindow.dismiss()
        }

    }
    private fun openPopupDelete(
        layoutInflater: LayoutInflater,
        viewModel: AlarmViewModel,
        title: String,
        message: String,
        buttonText:String,
        id: Int,
        tag: String,
    ):PopupWindow {
        val binding = PopupTakeRequestBinding.inflate(layoutInflater)
        val popupView = binding.root
        val width = LinearLayout.LayoutParams.MATCH_PARENT
        val height = LinearLayout.LayoutParams.WRAP_CONTENT
        val focusable = false
        val popupWindow = PopupWindow(popupView, width, height, focusable)
        popupWindow.showAtLocation(popupView, Gravity.BOTTOM, 0, 0)

        binding.textTitle.text = title
        binding.textMessage.text = message
        binding.buttonOk.text = buttonText
        deleteWindowButtonsClicked(binding, popupWindow, viewModel, id,tag)
        return popupWindow
    }
    private fun deleteWindowButtonsClicked(
        binding: PopupTakeRequestBinding, popupWindow: PopupWindow,
        viewModel: AlarmViewModel,id: Int,tag: String,
    ){

        binding.buttonCancel.setOnClickListener{
            popupWindow.dismiss()
        }

        binding.buttonOk.setOnClickListener{
            viewModel.deleteRow(id)
            viewModel.cancelWorkManager(tag)
            popupWindow.dismiss()
        }

    }

    private fun sendBroadCast(context: Context, key:String){
        val intent = Intent(key)
        intent.putExtra("message", "sendBroadCast")
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent)
    }
}