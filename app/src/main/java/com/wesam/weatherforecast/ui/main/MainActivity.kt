package com.wesam.weatherforecast.ui.main

import android.Manifest
import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.location.LocationManager
import android.net.ConnectivityManager
import android.os.Bundle
import android.os.Looper
import android.provider.Settings
import android.util.Log
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.lifecycle.ViewModelProvider
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.android.gms.location.*
import com.wesam.weatherforecast.R
import com.wesam.weatherforecast.databinding.ActivityMainBinding
import com.wesam.weatherforecast.model.data_classes.Weather
import com.wesam.weatherforecast.ui.BaseActivity
import com.wesam.weatherforecast.ui.favorite.FavoriteActivity
import com.wesam.weatherforecast.ui.map.MapFragment
import com.wesam.weatherforecast.ui.setting.SettingActivity
import com.wesam.weatherforecast.utils.*
import com.wesam.weatherforecast.utils.helper.*
import java.io.IOException
import java.util.*


class MainActivity : BaseActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: MainViewModel

    private var latitude: Double? = null
    private var longitude: Double? = null
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private lateinit var geoCoder: Geocoder
    private lateinit var conMgr: ConnectivityManager
    private  var weather: Weather? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        viewModel = ViewModelProvider(
            this, ViewModelProvider.AndroidViewModelFactory(application)
        ).get(MainViewModel::class.java)

        init()
        setContentView(binding.root)

    }

    private fun init() {
        fusedLocationProviderClient =
            LocationServices.getFusedLocationProviderClient(this)
        geoCoder = Geocoder(this, Locale.getDefault())
        conMgr = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager



        binding.viewModel = viewModel
        viewModel.getCurrentWeather()
        observeData()
        registerBroadcast()
    }

    private fun observeData() {
        observeWeather()
        observeNavigateToFavorite()
        observeNavigateToSetting()
        observeLocation()

    }


    @SuppressLint("SetTextI18n")
    private fun observeWeather() {
        viewModel.weather.observe(this, {
            if (it != null) {
                weather = it
                viewModel.getCityName()
            }
        })
    }


    private fun observeNavigateToSetting() {
        viewModel.navigateToSetting.observe(this, {
            Log.i(TAG, it.toString())
            it.getContentIfNotHandled()?.let {
                navigateActivity(this, SettingActivity())
            }
        })
    }

    private fun observeNavigateToFavorite() {
        viewModel.navigateToFavorite.observe(this, {
            Log.i(TAG, it.toString())
            it.getContentIfNotHandled()?.let {
                navigateActivity(this, FavoriteActivity())
            }
        })
    }

    private fun observeLocation(){
        viewModel.noLocationOptionFounded.observe(this,{
            it.getContentIfNotHandled()?.let {it1->
                Log.i(TAG,"LocationOptionFounded $it1")
                    openLocationChooses(this)
            }
        })

        viewModel.mapLocationOption.observe(this,{
            it.getContentIfNotHandled()?.let {it1->
                Log.i(TAG,"LocationOptionMapFounded $it1")
                openLocationMap(this)
            }
        })


        viewModel.gpsLocationOption.observe(this,{
            it.getContentIfNotHandled()?.let {it1->
                Log.i(TAG,"LocationOptionGPSFounded $it1")
                Log.i(TAG, "GetCurrentLocation gpsLocationOption")
                        getCurrentLocation()
            }
        })
    }


    private fun registerBroadcast(){
        LocalBroadcastManager.getInstance(this).registerReceiver(
            mLangReceiver,
            IntentFilter(CURRENT_LANGUAGE)
        )
        LocalBroadcastManager.getInstance(this).registerReceiver(
            mTempReceiver,
            IntentFilter(KEY_TEMP_UNIT_BROADCAST)
        )
        LocalBroadcastManager.getInstance(this).registerReceiver(
            mWindReceiver,
            IntentFilter(KEY_WIND_UNIT_BROADCAST)
        )
        LocalBroadcastManager.getInstance(this).registerReceiver(
            mCurrentReceiver,
            IntentFilter(KEY_CURRENT_POSITION_FAVORITE_BROADCAST)
        )
        LocalBroadcastManager.getInstance(this).registerReceiver(
            mLocationReceiver,
            IntentFilter(KEY_CHANGE_LOCATION_BROADCAST)
        )
        LocalBroadcastManager.getInstance(this).registerReceiver(
            mMapPositionReceiver,
            IntentFilter(KEY_CHANGE_MAP_LOCATION_BROADCAST)
        )

    }
    private fun getCurrentLocation() {
        Log.i(TAG, "GetCurrentLocation")
        if (checkedPermission()) {
            Log.i(TAG, "GetCurrentLocation checkedPermission")
            if (isLocationEnable()) {
                //flag = true
                Log.i(TAG, "GetCurrentLocation isLocationEnable")
                fusedLocationProviderClient.lastLocation.addOnCompleteListener {
                    val location: Location? = it.result
                    if (location == null) {
//                        if(networkConnection(conMgr,this)){
//                            getCountryName()
//                        }else{
//                            showShortMessage(this,"network connection is required")
//                        }
                        requestNewLocation()
                    } else {
                        // set lat and long
                        Log.i(TAG, "GetCurrentLocation ${location.latitude} ${location.longitude}")
                        latitude = location.latitude
                        longitude = location.longitude
                        if(networkConnection(conMgr,this)){
                            getCountryName()
                        }else{
                            showShortMessage(this,"network connection is required")
                        }
                    }
                }
            } else {
                showAlertPermissionRequired(this, Settings.ACTION_LOCATION_SOURCE_SETTINGS)
            }
        } else {
            requestPermission()
        }

    }

    private fun checkedPermission(): Boolean {
        return (ActivityCompat.checkSelfPermission(
            this,
            Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
            this,
            Manifest.permission.ACCESS_COARSE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED
                )
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            ),
            PERMISSION_ID
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        Log.i(TAG, "getCurrentLocation onRequestPermissionsResult")
        if (requestCode == PERMISSION_ID) {
            if(grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED ) {

                Log.i(TAG, "getCurrentLocation onRequestPermissionsResult")
                //flag = false
                viewModel.setLocationOption(DEFAULT_LOCATION_STRING)
                //getCurrentLocation()

            } else if (ActivityCompat.shouldShowRequestPermissionRationale(
                        this,
                        Manifest.permission.ACCESS_COARSE_LOCATION)){
                Log.i(TAG, "OnRequestPermissionsResult need permission")
                Log.i(TAG, "PERMISSION_DENIED PERMISSION_DENIED")
                showLongToast(this, "please need to set permission request")
                }else {
                viewModel.setLocationOption(MAP_LOCATION_STRING)
            }

        }
    }

    private fun isLocationEnable(): Boolean {
        val locationManager: LocationManager = getSystemService(Context.LOCATION_SERVICE)
                as LocationManager

        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
    }

    @SuppressLint("MissingPermission")
    private fun requestNewLocation() {
        val locationRequest = LocationRequest()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = 0
        locationRequest.fastestInterval = 0
        locationRequest.numUpdates = 1

        fusedLocationProviderClient.requestLocationUpdates(
            locationRequest, locationCallBack, Looper.myLooper()
        )

    }

    private val locationCallBack: LocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            super.onLocationResult(locationResult)
            val mLocation: Location = locationResult.lastLocation
            // get lat and long

            Log.i(TAG, "locationCallBack ${mLocation.altitude} ${mLocation.longitude}")

            latitude = mLocation.latitude
            longitude = mLocation.longitude
        }
    }


    private fun getCountryName() {
        if (latitude != null && longitude != null) {
            try {
                val addresses: List<Address> = geoCoder.getFromLocation(latitude!!, longitude!!, 1)
                val cityName = addresses[0].locality
                Log.i(TAG, cityName)

                if(cityName != null) {
                    val cityFromDB = viewModel.getCityName(cityName)
                    Log.i(TAG, "countryName $cityFromDB")

                    if (cityFromDB != cityName) {
                        val weatherObject =
                            Weather(countryName = cityName, latitude = latitude!!.toFloat(), longitude = longitude!!.toFloat())
                        viewModel.insertCountry(weatherObject)
                        showShortMessage(this,"position added successfully")
                    }
                    Log.i(TAG, "getCountryName")
                    viewModel.setLatLongToPref(latitude!!, longitude!!)
                    viewModel.getCurrentWeather()
                    viewModel.setCityName(cityName)
                   // viewModel.getCurrentWeather()
                }else{
                    showLongToast(this,"make sure you choose correct position")
                }
            }catch (e:IOException){
                Log.e(TAG,"IOException${e.localizedMessage}")
            }


        }

    }

    companion object {
        const val PERMISSION_ID = 1001
        const val TAG = "MainActivity"
    }

    override fun onResume() {
        super.onResume()
        Log.i(TAG, "onResume")

        viewModel.getLocationOption()
    }

    private val mLangReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            // Get extra data included in the Intent
            val message = intent.getStringExtra("message")
            Log.i("receiver", "Got message: $message")
            viewModel.setLang()
            recreate()
        }
    }
    private val mTempReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            // Get extra data included in the Intent
            //val message = intent.getStringExtra("message")
           viewModel.setTemp(weather?.current?.temperature)
            Log.i(TAG,"onReceive ${weather?.current?.temperature}")
            Log.i(TAG,"onReceive")
        }
    }
    private val mWindReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            // Get extra data included in the Intent
            //val message = intent.getStringExtra("message")
            viewModel.setWindSpeed(weather?.current?.wind_speed)
            Log.i(TAG,"onReceive ${weather?.current?.wind_speed}")
            Log.i(TAG,"onReceive")
        }
    }

    private val mCurrentReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            viewModel.clearData()
            viewModel.clearCity()
        }
    }
    private val mMapPositionReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            viewModel.getCurrentWeather()
        }
    }

    private val mLocationReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            Log.i(TAG, "onResume mLocationReceiver")
            viewModel.clearCity()

        }
    }


    override fun onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mLangReceiver)
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mTempReceiver)
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mWindReceiver)
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mCurrentReceiver)
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mLocationReceiver)
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMapPositionReceiver)
        super.onDestroy()
    }
   private fun openLocationChooses(activity: AppCompatActivity) {
        val dialogBuilder = AlertDialog.Builder(activity)
        val inflater = activity.layoutInflater
        val dialogView = inflater.inflate(R.layout.permission_dialog, null)
        dialogBuilder.setView(dialogView)

        val cancelButton = dialogView.findViewById(R.id.text_cancel) as Button
        val enableButton = dialogView.findViewById(R.id.text_enable) as Button
        val message = dialogView.findViewById(R.id.text_details) as TextView
        val title = dialogView.findViewById(R.id.text_title) as TextView

        title.text = activity.getString(R.string.need_to_locate)
        message.text = activity.getString(R.string.choose_location_option)
        enableButton.text = activity.resources.getString(R.string.gps)
        cancelButton.text = activity.resources.getString(R.string.map)


        val alertDialog = dialogBuilder.create()

        cancelButton.setOnClickListener {
            alertDialog.dismiss()
            navigateActivityMap(activity, MapFragment(),true)
            //viewModel.setLocationOption(MAP_LOCATION_STRING)
        }
        enableButton.setOnClickListener {
            alertDialog.dismiss()
            //viewModel.setLocationOption(DEFAULT_LOCATION_STRING)
            getCurrentLocation()
            Log.i(TAG, "GetCurrentLocation alertDialog")
        }
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alertDialog.show()
    }

}
