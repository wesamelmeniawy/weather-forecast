package com.wesam.weatherforecast.ui.popup

import android.content.Context
import android.view.LayoutInflater
import android.widget.PopupWindow
import com.wesam.weatherforecast.ui.alarm.AlarmViewModel
import com.wesam.weatherforecast.ui.favorite.FavoriteViewModel
import com.wesam.weatherforecast.ui.setting.SettingActivity
import com.wesam.weatherforecast.ui.setting.SettingViewModel

interface PopupRepo {
    fun openPopupTempWindow(
        layoutInflater: LayoutInflater,
        viewModel: SettingViewModel,
        context: Context
    ): PopupWindow

    fun openPopupLangWindow(
        layoutInflater: LayoutInflater,
        viewModel: SettingViewModel,
        activity: SettingActivity,
        context: Context
    ): PopupWindow

    fun openPopupLocationWindow(
        layoutInflater: LayoutInflater,
        viewModel: SettingViewModel,
        context: Context
    ): PopupWindow

    fun openPopupWindWindow(
        layoutInflater: LayoutInflater,
        viewModel: SettingViewModel,
        context: Context
    ): PopupWindow
    
    fun openRequestPopupWindow(
        layoutInflater: LayoutInflater,
        viewModel: FavoriteViewModel,
        title: String,
        message: String,
        buttonText: String,
        id: Int,
        context: Context,
        currentCityName: String,
        positionedCityName: String
        ): PopupWindow

    fun openDeletePopupWindow(
        layoutInflater: LayoutInflater,
        viewModel: AlarmViewModel,
        title: String,
        message: String,
        buttonText: String,
        id: Int,
        tag: String,
    ): PopupWindow
}