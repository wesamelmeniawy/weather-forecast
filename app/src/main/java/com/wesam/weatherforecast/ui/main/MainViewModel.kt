package com.wesam.weatherforecast.ui.main

import android.app.Application
import android.util.Log
import android.view.View
import androidx.lifecycle.*
import com.wesam.weatherforecast.model.ModelRepository
import com.wesam.weatherforecast.model.data_classes.Daily
import com.wesam.weatherforecast.model.data_classes.Hourly
import com.wesam.weatherforecast.model.data_classes.Weather
import com.wesam.weatherforecast.utils.*
import com.wesam.weatherforecast.utils.helper.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.*
import kotlin.collections.ArrayList

class MainViewModel(application: Application) : AndroidViewModel(application) {

    private val _weather  = MutableLiveData<Weather?>()
    private val _navigateToSetting  = MutableLiveData<Event<Boolean>>()
    private val _navigateToFavorite  = MutableLiveData<Event<Boolean>>()
    private val _noLocationOptionFounded  = MutableLiveData<Event<Boolean>>()
    private val _mapLocationOption = MutableLiveData<Event<Boolean>>()
    private val _gpsLocationOption  = MutableLiveData<Event<Boolean>>()




    private var _textCurrentDate = MutableLiveData<String>()
    private var _humidity = MutableLiveData<String>()
    private var _clouds = MutableLiveData<String>()
    private var _pressure = MutableLiveData<String>()
    private var _windSpeed = MutableLiveData<String>()
    private var _countryName = MutableLiveData<String>()
    private var _temperature = MutableLiveData<String>()
    private var _description = MutableLiveData<String>()
    private var _progressVisibility = MutableLiveData<Int>()
    private var _constantVisibility = MutableLiveData<Int>()
    private var _animationVisibility = MutableLiveData<Int>()


    private var _tempUnit: String
    private var _windUnit: String
    private var _langType: String
    private val _modelRepository: ModelRepository = ModelRepository(application.applicationContext)
    private var dailyList: List<Daily?> = ArrayList()
    private var hourlyList: List<Hourly?> = ArrayList()
    private var list: List<String> = ArrayList()

    var dailyAdapter: DailyAdapter
    var hourlyAdapter: HourlyAdapter


    val weather:LiveData<Weather?>
        get() = _weather


    val navigateToSetting:LiveData<Event<Boolean>>
        get() = _navigateToSetting

    val navigateToFavorite:LiveData<Event<Boolean>>
        get() = _navigateToFavorite

    val noLocationOptionFounded:LiveData<Event<Boolean>>
        get() = _noLocationOptionFounded

    val mapLocationOption:LiveData<Event<Boolean>>
        get() = _mapLocationOption

    val gpsLocationOption:LiveData<Event<Boolean>>
        get() = _gpsLocationOption


    val humidity:LiveData<String>
        get() = _humidity

    val clouds:LiveData<String>
        get() = _clouds

    val pressure:LiveData<String>
        get() = _pressure

    val windSpeed:LiveData<String>
        get() = _windSpeed

    val countryName:LiveData<String>
        get() = _countryName

    val temperature:LiveData<String>
        get() = _temperature
    val description:LiveData<String>
        get() = _description

    val progressVisibility:LiveData<Int>
        get() = _progressVisibility

    val constantVisibility:LiveData<Int>
        get() = _constantVisibility

    val animationVisibility:LiveData<Int>
        get() = _animationVisibility

    val textCurrentDate:LiveData<String>
    get() = _textCurrentDate



    init {
        val countryNameFromPref = _modelRepository.getPositionedCountryName()
        Log.i("countyName",countryNameFromPref)
        setCountryName(countryNameFromPref)

        _constantVisibility.postValue(View.GONE)
        _animationVisibility.postValue(View.VISIBLE)

        _modelRepository.getLocationOption()
        _modelRepository.getTempUnit()
        _modelRepository.getWindUnit()
        _tempUnit = _modelRepository.getTempUnit()
        _windUnit = _modelRepository.getWindUnit()
        _langType = _modelRepository.getLanguage()
        dailyAdapter = DailyAdapter(_tempUnit,_langType)
        hourlyAdapter = HourlyAdapter(_tempUnit,_langType)

    }

    fun insertCountry(weather:Weather){
        viewModelScope.launch(Dispatchers.IO) {
            _modelRepository.insertCountry(weather)
        }
    }

    fun getCurrentWeather(){
        Log.i(TAG, "Latitude ${_modelRepository
            .getPositionLatitude()}")
        Log.i(TAG, "Longitude ${_modelRepository
            .getPositionLongitude()}")
        _progressVisibility.postValue(View.VISIBLE)
        _animationVisibility.postValue(View.VISIBLE)
        _constantVisibility.postValue(View.GONE)
        viewModelScope.launch(Dispatchers.IO) {
            Log.i(TAG, "Lang ${_modelRepository.getLanguage()}")
            var response:Weather? = null
            when (_modelRepository.getLanguage()) {
                ""->{ response = _modelRepository.getWeather(_modelRepository
                    .getPositionLatitude(),
                    _modelRepository
                        .getPositionLongitude(), "en",
                    _modelRepository.getPositionedCountryName())}
                DEFAULT_LANG_STRING ->{ response = _modelRepository.getWeather(_modelRepository
                    .getPositionLatitude(),
                    _modelRepository
                        .getPositionLongitude(), "en",
                    _modelRepository.getPositionedCountryName())}

                ARABIC_LANG_STRING -> { response = _modelRepository.getWeather(_modelRepository
                    .getPositionLatitude(),
                    _modelRepository
                        .getPositionLongitude(), "ar",
                    _modelRepository.getPositionedCountryName())}
            }
            Log.i(TAG,"response $response")
            if(response != null){
                Log.i("response", response.toString())
                _weather.postValue(response)
                if(response.current != null){
                    viewModelScope.launch(Dispatchers.Main) {
                        setData(response)
                    }
                }
            }
            _progressVisibility.postValue(View.GONE)

        }
    }


    private fun setCountryName(countyName: String): String {
        Log.i(TAG,"countyName $countyName")

        val countyNamePlaceHolder:String = if(countyName == ""){
           "Current Position"
       }else{
           _modelRepository.getPositionedCountryName()
       }
        Log.i("setCountryName",countyNamePlaceHolder )
        _countryName.postValue(countyNamePlaceHolder)
        return countyNamePlaceHolder
    }

    fun setCityName(countyName: String){
        _modelRepository.setPositionedCountryName(countyName)
        _countryName.postValue(countyName)
    }

    fun setLatLongToPref(latitude: Double, longitude: Double){
        _modelRepository.setPositionLatitude(latitude.toFloat())
        _modelRepository.setPositionLongitude(longitude.toFloat())
    }


    fun settingButtonClicked(){
        _navigateToSetting.postValue(Event(true))
        Log.i(TAG,"settingButtonClicked" )
    }
    fun getCityName(){
        setCountryName( _modelRepository.getPositionedCountryName())
    }

    fun cityButtonClicked(){
        _navigateToFavorite.postValue(Event(true))
        Log.i(TAG,"cityButtonClicked" )
    }

    fun getLocationOption(){
        getLang()
        when (_modelRepository.getLocationOption()) {
            "" -> {
                _noLocationOptionFounded.postValue(Event(true))
                _progressVisibility.postValue(View.GONE)
            }
            DEFAULT_LOCATION_STRING -> {
                Log.i(TAG,"getPositionedCountryName ${_modelRepository.getPositionedCountryName()}" )
                if(_modelRepository.getPositionedCountryName() == ""){
                    _progressVisibility.postValue(View.GONE)
                    _gpsLocationOption.postValue(Event(true))
                }
            }
            MAP_LOCATION_STRING -> {
                if(_modelRepository.getPositionedCountryName() == "") {
                    _progressVisibility.postValue(View.GONE)
                    _mapLocationOption.postValue(Event(true))
                }
            }
        }
    }
    fun setLocationOption(locationOption: String){
        _modelRepository.setLocationOption(locationOption)
    }
    fun clearCity(){
        _modelRepository.setPositionedCountryName("")
        _modelRepository.setPositionLatitude(0f)
        _modelRepository.setPositionLongitude(0f)
        _constantVisibility.postValue(View.GONE)
        setCountryName("")
    }

     private fun setData(response: Weather?){
         _animationVisibility.postValue(View.GONE)
         response?.let {
             getLang()
             list = if (_modelRepository.getLanguage() == DEFAULT_LANG_STRING){
                 splitDate(dateFormat(response.current?.date,Locale.ENGLISH),Locale.ENGLISH)

             }else{
                 //splitDate(dateFormat(response.current?.date, Locale("ar")),Locale("ar"))
                 splitDate(dateFormat(response.current?.date, Locale("ar")),Locale.ENGLISH)
             }

             if(list.size >1){
                 _textCurrentDate.postValue("${list[2]} ${list[1]}")
             }
             setTemp(response.current?.temperature)
             setWindSpeed(response.current?.wind_speed)
             response.let { hourlyAdapter.loadData(it.hourly,_tempUnit,_langType) }
             response.let { dailyAdapter.loadData(it.daily,_tempUnit,_langType) }
             dailyList = response.daily
             hourlyList = response.hourly

             if(response.current != null){
                 _constantVisibility.postValue(View.GONE)
                 _humidity.postValue("${response.current.humidity} %")
                 _clouds.postValue("${response.current.clouds} %")
                 _pressure.postValue("${response.current.pressure} " +
                         "${setPressure(_modelRepository.getLanguage())} ")
                 _description.postValue("${response.current.weather?.get(0)?.description} ")
                 _constantVisibility.postValue(View.VISIBLE)
                 _progressVisibility.postValue(View.GONE)
             }

         }

    }
     fun clearData(){
        _constantVisibility.postValue(View.GONE)
         _animationVisibility.postValue(View.VISIBLE)
    }

    fun setTemp(temp: Double?){
        _tempUnit = _modelRepository.getTempUnit()

        if(_tempUnit.isBlank()){
            _tempUnit = DEFAULT_TEMP_STRING
        }

        if(temp != null){
            val tempUnitString = calTempUnit(temp,_tempUnit)
            _temperature.postValue(tempUnitString)

            dailyAdapter.loadData(dailyList,_tempUnit,_langType)
            hourlyAdapter.loadData(hourlyList, _tempUnit,_langType)
        }

    }
    fun setLang(){
        _langType = _modelRepository.getLanguage()
    }

    fun setWindSpeed(windSpeed: Float?){
        getLang()
        _windUnit = _modelRepository.getWindUnit()

        if(_windUnit.isBlank()){
            _windUnit = DEFAULT_WIND_STRING
        }

        if(windSpeed != null){
            val windSpeedUnitString = calWindSpeedUnit(windSpeed,_windUnit,_langType)
            _windSpeed.postValue(windSpeedUnitString)
        }
    }
    fun getCityName(cityName:String):String{
        val countryList =  _modelRepository.getAllCountry()
        var countryName = " city not found"
        for(i in countryList){
            if (cityName == i){
                countryName = i
                break
            }
        }
        return countryName
    }

   private fun getLang(){
        if(_modelRepository.getLanguage().isBlank()){
            _modelRepository.setLanguage(DEFAULT_LANG_STRING)
        }
    }

    companion object{
        const val TAG = "MainViewModel"
    }

}