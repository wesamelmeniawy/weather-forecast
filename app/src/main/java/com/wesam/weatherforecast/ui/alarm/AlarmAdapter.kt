package com.wesam.weatherforecast.ui.alarm

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.wesam.weatherforecast.R
import com.wesam.weatherforecast.databinding.AlertRowItemBinding
import com.wesam.weatherforecast.model.data_classes.Alarm
import com.wesam.weatherforecast.utils.*

class AlarmAdapter (private val listener: AlarmViewModel, private val lang:String):
    RecyclerView.Adapter<AlarmAdapter.ViewHolder>(){

    private var alarmList: List<Alarm?> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val viewBinding = AlertRowItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent, false
        )
        return ViewHolder(viewBinding)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        //holder.binding.textAlarmType.text = alarmList[position]?.eventType
        holder.binding.textDay.text = alarmList[position]?.date
        holder.binding.textFrom.text = alarmList[position]?.timeFrom
        holder.binding.textTo.text = alarmList[position]?.timeTo

        if(alarmList[position]?.active == false){
            holder.binding.imageNotification.setImageResource(
                R.drawable.ic_baseline_notifications_off_24)
        }else{
            holder.binding.imageNotification.setImageResource(
                R.drawable.ic_baseline_notifications_24)
        }

        if(alarmList[position]?.alarm == true){
            holder.binding.imageAlarm.setImageResource(
                R.drawable.ic_alarm)
        }else{
            holder.binding.imageAlarm.setImageResource(
                R.drawable.ic_alarm_off)
        }

        if(lang == DEFAULT_LANG_STRING){
            when(alarmList[position]?.eventType){
                HUMIDITY_ARABIC-> holder.binding.textAlarmType.text = HUMIDITY_ENGLISH
                CLOUDS_ARABIC-> holder.binding.textAlarmType.text = CLOUDS_ENGLISH
                TEMPERATURE_ARABIC-> holder.binding.textAlarmType.text = TEMPERATURE_ENGLISH
                WIND_SPEED_ARABIC-> holder.binding.textAlarmType.text = WIND_SPEED_ENGLISH
                WIND_DIRECTION_ARABIC-> holder.binding.textAlarmType.text = WIND_DIRECTION_ENGLISH
                PRESSURE_ARABIC-> holder.binding.textAlarmType.text = PRESSURE_ENGLISH
                else-> holder.binding.textAlarmType.text = alarmList[position]?.eventType
            }
        }else if(lang == ARABIC_LANG_STRING){
            when(alarmList[position]?.eventType){
                HUMIDITY_ENGLISH-> holder.binding.textAlarmType.text = HUMIDITY_ARABIC
                CLOUDS_ENGLISH-> holder.binding.textAlarmType.text = CLOUDS_ARABIC
                TEMPERATURE_ENGLISH-> holder.binding.textAlarmType.text = TEMPERATURE_ARABIC
                WIND_SPEED_ENGLISH-> holder.binding.textAlarmType.text = WIND_SPEED_ARABIC
                WIND_DIRECTION_ENGLISH-> holder.binding.textAlarmType.text = WIND_DIRECTION_ARABIC
                PRESSURE_ENGLISH-> holder.binding.textAlarmType.text = PRESSURE_ARABIC
                else-> holder.binding.textAlarmType.text = alarmList[position]?.eventType
            }
        }

    }
    inner class ViewHolder(var binding: AlertRowItemBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener{
        init {
            binding.imgDelete.setOnClickListener(this)
            binding.imageNotification.setOnClickListener(this)
            binding.imageAlarm.setOnClickListener(this)
        }
        override fun onClick(v: View?) {
            val position = adapterPosition
            when(v?.id){
                R.id.img_delete ->{listener.deleteItemClicked(position)}
                R.id.image_notification ->{listener.changeNotification(alarmList[position]?.active, alarmList[position]?.id)}
                R.id.image_alarm ->{listener.changeAlarmStatus(alarmList[position]?.alarm, alarmList[position]?.id)}
            }

        }

    }

    override fun getItemCount(): Int {
        return alarmList.size
    }

    fun loadData(alarmList: List<Alarm?>) {
        this.alarmList = alarmList
        notifyDataSetChanged()
    }
}