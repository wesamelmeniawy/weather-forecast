package com.wesam.weatherforecast.ui.alarm

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.PopupWindow
import androidx.lifecycle.ViewModelProvider
import com.wesam.weatherforecast.R
import com.wesam.weatherforecast.databinding.ActivityAlarmBinding
import com.wesam.weatherforecast.model.ModelRepository
import com.wesam.weatherforecast.model.data_classes.Alarm
import com.wesam.weatherforecast.ui.BaseActivity
import com.wesam.weatherforecast.ui.favorite.FavoriteActivity
import com.wesam.weatherforecast.ui.popup.PopupRepo
import com.wesam.weatherforecast.ui.popup.PopupRepository

class AlarmActivity : BaseActivity(){

    private lateinit var binding: ActivityAlarmBinding
    private lateinit var viewModel: AlarmViewModel

    private lateinit var alarmAdapter: AlarmAdapter
    private lateinit var alarmList: List<Alarm>
    //private lateinit var alarmActiveList: List<Alarm>
    private lateinit var _modelRepository : ModelRepository

    private  var popupRequestWindow: PopupWindow? = null
    private lateinit var popupRepo: PopupRepo

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAlarmBinding.inflate(layoutInflater)

        viewModel = ViewModelProvider(
            this, ViewModelProvider.AndroidViewModelFactory(application)
        ).get(AlarmViewModel::class.java)

        init()

        setContentView(binding.root)
    }

    private fun init(){
        binding.viewMode = viewModel

       _modelRepository = ModelRepository(application.applicationContext)
        alarmAdapter = AlarmAdapter(viewModel,_modelRepository.getLanguage())
        binding.recyclerAlarm.adapter = alarmAdapter

        popupRepo = PopupRepository(application)

        observeData()
        buttonBackClicked()

    }

    private fun observeData(){
        observeFab()
        observeGetAlarms()
        observeDeleteAlarm()
        observeNotificationActive()
        observeNotificationNotActive()
        observeAlarmActive()
        observeAlarmNotActive()
        //observeGetActiveAlarms()
    }

    private fun observeFab(){
        viewModel.addAlarmFab.observe(this,{
            it.getContentIfNotHandled()?.let {
                //navigateActivity(this,AddAlarmActivity())
                val dialog = DialogAddAlarm.newInstance()
                dialog.show(this.supportFragmentManager,"DialogAddAlarm")
            }
        })

    }

    private fun observeGetAlarms(){
        viewModel.getAllAlarm().observe(this,{
            if (it.isNotEmpty()){
                binding.recyclerAlarm.visibility = View.VISIBLE
                binding.emptyList.visibility = View.GONE
                binding.txtEmpty.visibility = View.GONE
                Log.i(FavoriteActivity.TAG, it.toString())
                alarmAdapter.loadData(it)
                alarmList = it
            }else{
                binding.recyclerAlarm.visibility = View.GONE
                binding.emptyList.visibility = View.VISIBLE
                binding.txtEmpty.visibility = View.VISIBLE
            }
        })
    }
//    private fun observeGetActiveAlarms(){
//        viewModel.getActiveAlarm().observe(this,{
//            //alarmAdapter.loadData(it)
//            //alarmActiveList = it
//        })
//    }
    private fun observeDeleteAlarm(){
        viewModel.showDeleteItemDialog.observe(this, {
            it.getContentIfNotHandled()?.let { it1 ->
                popupRequestWindow = alarmList[it1].id?.let { it2 ->
                    alarmList[it1].workManagerTag?.let { it3 ->
                        popupRepo.openDeletePopupWindow(
                            layoutInflater,
                            viewModel,
                            resources.getString(R.string.item_delete),
                            resources.getString(R.string.message_sure),
                            resources.getString(R.string.ok),
                            it2,
                            it3
                        )
                    }
                }
            }
        })
    }
    private fun observeNotificationActive(){
        viewModel.notificationActive.observe(this,{
            it.getContentIfNotHandled()?.let {
                Log.i(TAG,"active $alarmList")
                alarmAdapter.loadData(alarmList)
            }
        })

    }
    private fun observeNotificationNotActive(){
        viewModel.notificationNotActive.observe(this,{
            it.getContentIfNotHandled()?.let {
                Log.i(TAG,"not active $alarmList")
                alarmAdapter.loadData(alarmList)
            }
        })
    }

    private fun observeAlarmActive(){
        viewModel.alarmActive.observe(this,{
            it.getContentIfNotHandled()?.let {
                Log.i(TAG,"alarm $alarmList")
                alarmAdapter.loadData(alarmList)
            }
        })

    }
    private fun observeAlarmNotActive(){
        viewModel.alarmNotActive.observe(this,{
            it.getContentIfNotHandled()?.let {
                Log.i(TAG,"not alarm $alarmList")
                alarmAdapter.loadData(alarmList)
            }
        })
    }
    private fun buttonBackClicked(){
        binding.layoutToolbarBinding.imgBack.setOnClickListener {
            finish()
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        popupRequestWindow?.dismiss()
    }

    companion object{
        const val TAG = "AlarmActivity"
    }
}