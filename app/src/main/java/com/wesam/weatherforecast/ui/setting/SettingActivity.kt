package com.wesam.weatherforecast.ui.setting

import android.os.Bundle
import android.util.Log
import android.widget.PopupWindow
import androidx.lifecycle.ViewModelProvider
import com.wesam.weatherforecast.databinding.ActivitySettingBinding
import com.wesam.weatherforecast.ui.BaseActivity
import com.wesam.weatherforecast.ui.alarm.AlarmActivity
import com.wesam.weatherforecast.ui.popup.PopupRepo
import com.wesam.weatherforecast.ui.popup.PopupRepository
import com.wesam.weatherforecast.utils.helper.navigateActivity


class SettingActivity : BaseActivity(){
    private lateinit var binding: ActivitySettingBinding
    private lateinit var viewModel: SettingViewModel

    private  var popupTempWindow:PopupWindow? = null
    private  var popupLangWindow:PopupWindow? = null
    private  var popupLocationWindow:PopupWindow? = null
    private  var popupWindWindow:PopupWindow? = null

    private lateinit var popupRepo: PopupRepo
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySettingBinding.inflate(layoutInflater)

        init()
        setContentView(binding.root)
    }

    private fun init(){
        viewModel = ViewModelProvider(
            this, ViewModelProvider.AndroidViewModelFactory(application)
        ).get(SettingViewModel::class.java)
        popupRepo= PopupRepository(application)

        binding.viewModel = viewModel
        viewModel.langString()
        viewModel.windString()
        viewModel.tempString()
        viewModel.locationString()
        buttonsClicked()
        observeItem()
    }

    private fun buttonsClicked(){
        binding.layoutToolbarBinding.imgBack.setOnClickListener{
            finish()
        }
    }

    private fun observeItem(){
        viewModel.tempUnitClicked.observe(this, {
            Log.i(TAG, "Temp Item Activity${it}")
            it.getContentIfNotHandled()?.let {
                popupTempWindow = popupRepo.openPopupTempWindow(
                    layoutInflater,
                    viewModel,
                    this
                )
            }

        })

        viewModel.windUnitClicked.observe(this, {
            it.getContentIfNotHandled()?.let {
                popupTempWindow = popupRepo.openPopupWindWindow(
                    layoutInflater,
                    viewModel,
                    this
                )

            }
        })

        viewModel.locationClicked.observe(this, {
            it.getContentIfNotHandled()?.let {
                popupTempWindow = popupRepo.openPopupLocationWindow(
                    layoutInflater,
                    viewModel,
                    this
                )
            }
        })
        viewModel.languageClicked.observe(this, {
            it.getContentIfNotHandled()?.let {
                popupLangWindow = popupRepo.openPopupLangWindow(
                    layoutInflater,
                    viewModel,
                    this,
                    this
                )
            }
        })

        viewModel.language.observe(this, {
            binding.lanItem.value = it
        })
        viewModel.locationOption.observe(this, {
            binding.locationItem.value = it
        })
        viewModel.tempUnit.observe(this, {
            binding.tempItem.value = it
        })
        viewModel.windUnit.observe(this, {
            binding.windSpeedItem.value = it
        })

        viewModel.navigateToAlarm.observe(this, {
            it.getContentIfNotHandled()?.let {
                navigateActivity(this, AlarmActivity())
            }
        })
    }



    override fun onBackPressed() {
        super.onBackPressed()
            popupTempWindow?.dismiss()
            popupLangWindow?.dismiss()
            popupLocationWindow?.dismiss()
            popupWindWindow?.dismiss()
    }
    companion object{
        const val TAG = "SettingActivity"
    }

}