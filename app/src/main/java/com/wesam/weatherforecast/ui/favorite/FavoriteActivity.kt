package com.wesam.weatherforecast.ui.favorite


import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.PopupWindow
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.squareup.moshi.JsonAdapter
import com.wesam.weatherforecast.R
import com.wesam.weatherforecast.databinding.ActivityFavoriteBinding
import com.wesam.weatherforecast.model.ModelRepository
import com.wesam.weatherforecast.model.data_classes.Weather
import com.wesam.weatherforecast.ui.BaseActivity
import com.wesam.weatherforecast.ui.favoritdetails.CityDetailsActivity
import com.wesam.weatherforecast.ui.map.MapFragment
import com.wesam.weatherforecast.ui.popup.PopupRepo
import com.wesam.weatherforecast.ui.popup.PopupRepository
import com.wesam.weatherforecast.utils.*
import com.wesam.weatherforecast.utils.helper.navigateActivityMap
import com.wesam.weatherforecast.utils.helper.sendBroadCast


class FavoriteActivity : BaseActivity() {

    private lateinit var binding: ActivityFavoriteBinding
    private lateinit var viewModel:FavoriteViewModel

    private lateinit var favoriteAdapter: FavoriteAdapter
    private lateinit var favoriteList: List<Weather>
    private lateinit var tempUnit:String
    private val moshiObject = moshi

    private lateinit var popupRepo: PopupRepo
    private  var popupRequestWindow: PopupWindow? = null
    private lateinit var _modelRepository : ModelRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFavoriteBinding.inflate(layoutInflater)

        viewModel = ViewModelProvider(
            this, ViewModelProvider.AndroidViewModelFactory(application)
        ).get(FavoriteViewModel::class.java)
        binding.viewModel = viewModel

        init()
        setContentView(binding.root)
    }

    private fun init(){
        _modelRepository = ModelRepository(application.applicationContext)
        tempUnit = viewModel.getTempUnit()
        favoriteAdapter = FavoriteAdapter(viewModel,tempUnit)
        binding.recyclerFavorite.adapter = favoriteAdapter
        popupRepo = PopupRepository(application)

        viewModel.getAllWeather()

        LocalBroadcastManager.getInstance(this).registerReceiver(
            mMessageReceiver,
            IntentFilter(DELETE_ITEM_CANCEL)
        )
        observeData()
        buttonsClicked()
        swipeRecyclerView()
    }

    private fun observeData(){
        observeNavigateToMap()
        //observeButtonBackClicked()
        observeGetData()
        observeNavigateToShowDetails()
    }
    private fun observeGetData(){
        Log.i("observeGetData","observeGetData")
        viewModel.getAllWeather().observe(this, {
            if (it.isNotEmpty()){
                binding.recyclerFavorite.visibility = View.VISIBLE
                binding.emptyList.visibility = View.GONE
                binding.txtEmpty.visibility = View.GONE
                Log.i(TAG, it.toString())
                favoriteList = it
                favoriteAdapter.loadData(it, tempUnit)
            }else{
                viewModel.setCurrentPosition()
                binding.recyclerFavorite.visibility = View.GONE
                binding.emptyList.visibility = View.VISIBLE
                binding.txtEmpty.visibility = View.VISIBLE
                sendBroadCast(this, KEY_CURRENT_POSITION_FAVORITE_BROADCAST)
            }

        })
    }

    private fun observeNavigateToMap(){
        viewModel.navigateToMap.observe(this, {
            it.getContentIfNotHandled()?.let {
                navigateActivityMap(this,MapFragment(),false)
            }
        })
    }
    private fun observeNavigateToShowDetails(){
        viewModel.navigateToItemDetails.observe(this, {
            it.getContentIfNotHandled()?.let { it1 ->
                Log.i(TAG, "id $it1")
                navigateActivity(CityDetailsActivity(), it1)
            }
        })
    }
    private fun buttonsClicked(){
        binding.layoutToolbarBinding.imgBack.setOnClickListener{
            finish()
        }
    }

    private fun navigateActivity(activity: AppCompatActivity, id: Int) {
        val weatherString = convertWeatherToString(id)
        val intent = Intent(this, activity::class.java)
        intent.apply {
            putExtra(KEY_DETAILS_STRING, weatherString)
        }
        startActivity(intent)
    }
    private fun convertWeatherToString(id: Int): String{
        val adapterCurrent: JsonAdapter<Weather?> = moshiObject.adapter(Weather::class.java)
        return adapterCurrent.toJson(favoriteList[id])
    }

    private fun swipeRecyclerView(){
        val simpleItemTouchCallback: ItemTouchHelper.SimpleCallback = object :
            ItemTouchHelper.SimpleCallback(
                10,
                 ItemTouchHelper.RIGHT
            ) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                Log.i(TAG,"onMove")
                return false
            }
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, swipeDir: Int) {
                Log.i(TAG,"onSwiped ${viewHolder.adapterPosition}")
                //favoriteAdapter.loadData(favoriteList)
                favoriteList[viewHolder.adapterPosition].id?.let {
                    popupRequestWindow =
                        favoriteList[viewHolder.adapterPosition].countryName?.let { it1 ->
                            popupRepo.openRequestPopupWindow(layoutInflater,
                                viewModel,
                                resources.getString(R.string.item_delete),
                                resources.getString(R.string.message_sure),
                                resources.getString(R.string.ok),
                                it,
                                this@FavoriteActivity,
                                it1,
                                _modelRepository.getPositionedCountryName()
                            )
                        }
                }
            }
        }
        val itemTouchHelper = ItemTouchHelper(simpleItemTouchCallback)
        itemTouchHelper.attachToRecyclerView(binding.recyclerFavorite)
    }

    companion object{
        const val TAG = "FavoriteActivity"
    }
    private val mMessageReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            // Get extra data included in the Intent
            val message = intent.getStringExtra("message")
            Log.i("receiver", "Got message: $message")
           // recreate()
            favoriteAdapter.loadData(favoriteList, tempUnit)
        }
    }

    override fun onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver)
        super.onDestroy()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        Log.i("onBackPressed","onBackPressed")
            popupRequestWindow?.dismiss()
    }

}