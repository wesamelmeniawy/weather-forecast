package com.wesam.weatherforecast.ui.alarm

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.app.Dialog
import android.app.TimePickerDialog
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import com.wesam.weatherforecast.R
import com.wesam.weatherforecast.databinding.AddAlarmDialogBinding
import com.wesam.weatherforecast.model.data_classes.*
import com.wesam.weatherforecast.utils.helper.showLongToast
import com.wesam.weatherforecast.utils.helper.showShortMessage
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


class DialogAddAlarm: DialogFragment() {
    private lateinit var binding: AddAlarmDialogBinding
    private lateinit var viewModel: AddAlarmViewModel

    private lateinit var dateString:String
    private lateinit var timeFrom:String
    private lateinit var timeTo:String
    private var eventType:String = "Temperature"
    private  var apiEventType:String = "temp"
    private var repeated: Boolean? = false

    private var selectedYear:Int = 0
    private var selectedMonth:Int = 0
    private var selectedDay:Int = 0
    private var timeFromMiliSecond:Long = 1
    private var timeToMiliSecond:Long = 0

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val inflater = LayoutInflater.from(activity)

        binding = DataBindingUtil.inflate(
            inflater, R.layout.add_alarm_dialog, null, false
        )
        val builder = AlertDialog.Builder(activity!!)
        viewModel = ViewModelProvider(
            this, ViewModelProvider.AndroidViewModelFactory(activity!!.application)
        ).get(AddAlarmViewModel::class.java)

        init()

        builder.setView(binding.root)
        return builder.create()
    }

    private fun init(){

        binding.viewModel = viewModel
        observeData()
        checkBoxListener()
        setChipListener()

    }
    private fun observeData(){
        observeButtonAddAlarm()
        observeAddDate()
        observeAddTimeFrom()
        observeAddTimeTo()
        observeCancelDialog()
        observeAddTrigger()
        observeFailToAddTrigger()
    }

    private fun observeButtonAddAlarm(){
        viewModel.addAlarm.observe(this, {
            it.getContentIfNotHandled()?.let {
                checkData()
                //dismiss()
            }
        })
    }
    private fun observeAddDate(){
        viewModel.addDate.observe(this, {
            it.getContentIfNotHandled()?.let { it1 ->
                Log.i(TAG, "observeAddDate $it1")
                getDate()
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                    binding.textDateValue.setTextColor(
                        resources.getColor(
                            R.color.purple_200,
                            resources.newTheme()
                        )
                    )
                }
            }
        })

    }
    private fun observeAddTimeFrom(){
        viewModel.addTimeFrom.observe(this, {
            it.getContentIfNotHandled()?.let { it1 ->
                Log.i(TAG, "observeAddTimeFrom $it1")
                getFromTime()
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                    binding.textTimeFromValue.setTextColor(
                        resources.getColor(
                            R.color.purple_200,
                            resources.newTheme()
                        )
                    )
                }
            }
        })
    }
    private fun observeAddTimeTo(){
        viewModel.addTimeTo.observe(this, {
            it.getContentIfNotHandled()?.let { it1 ->
                Log.i(TAG, "observeAddTimeTo $it1")
                getToTime()
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                    binding.textTimeToValue.setTextColor(
                        resources.getColor(
                            R.color.purple_200,
                            resources.newTheme()
                        )
                    )
                }
            }
        })

    }
    private fun observeCancelDialog(){
        viewModel.cancel.observe(this, {
            it.getContentIfNotHandled()?.let {
                dismiss()
            }
        })
    }
    private fun observeAddTrigger(){
        viewModel.addTriggerSuccessfully.observe(this,{
            it.getContentIfNotHandled()?.let {
                showShortMessage(requireContext(),"Alarm added Successfully")
                dismiss()
            }
        })
    }
    private fun observeFailToAddTrigger(){
        viewModel.triggerAddedFailed.observe(this,{
            it.getContentIfNotHandled()?.let { it1->
                showShortMessage(requireContext(),it1)
            }
        })
    }
    private fun checkBoxListener(){
        binding.checkRepeated.setOnCheckedChangeListener { _, isChecked ->
            repeated = if(isChecked){
                Log.i(TAG, "checkBoxListener true")
                true
            }else{
                Log.i(TAG, "checkBoxListener false")
                false
            }
        }
    }
    private fun setChipListener(){
        binding.chipGroup.setOnCheckedChangeListener { _, checkedId ->
            when(checkedId){
                R.id.text_temp -> {
                    Log.i(TAG, "setChipListener temp")
                    eventType = resources.getString(R.string.temperature)
                    apiEventType = "temp"
                }
                R.id.text_pressure -> {
                    Log.i(TAG, "setChipListener pressure")
                    eventType = resources.getString(R.string.pressure)
                    apiEventType = "pressure"
                }
                R.id.text_humidity -> {
                    Log.i(TAG, "setChipListener humidity")
                    eventType = resources.getString(R.string.humidity)
                    apiEventType = "humidity"
                }
                R.id.text_wind_speed -> {
                    Log.i(TAG, "setChipListener wind speed")
                    eventType = resources.getString(R.string.wind_speed)
                    apiEventType = "wind_speed"
                }
                R.id.text_wind_direction -> {
                    Log.i(TAG, "setChipListener wind direction")
                    eventType = resources.getString(R.string.wind_direction)
                    apiEventType = "wind_direction"
                }
                R.id.text_clouds -> {
                    Log.i(TAG, "setChipListener cloud")
                    eventType = resources.getString(R.string.cloud)
                    apiEventType = "clouds"
                }
            }
        }
    }
    @SuppressLint("SimpleDateFormat")
    private fun getDate(){
        var date: Date? = null
        val onDateSetListener =
            OnDateSetListener { _, year, month, dayOfMonth ->
                val months = month + 1
                selectedYear = year
                selectedMonth = month
                selectedDay = dayOfMonth
                try {
                    date = SimpleDateFormat("dd/MM/yyyy").parse("$dayOfMonth/$months/$year")
                } catch (e: ParseException) {
                    e.printStackTrace()
                }
                date?.let {
                   val dateFormat = SimpleDateFormat("dd / MM / yyyy").format(it)
                    dateString = dateFormat
                    binding.textDateValue.text = dateFormat
                }
            }
        val calendar = Calendar.getInstance(TimeZone.getDefault())

            val dialog = DatePickerDialog(
                requireContext(), onDateSetListener,
                calendar[Calendar.YEAR], calendar[Calendar.MONTH],
                calendar[Calendar.DAY_OF_MONTH]
            )
            dialog.setTitle("Please select date.")
            dialog.datePicker.minDate = (System.currentTimeMillis() - 1000)
            dialog.show()
    }
    @SuppressLint("SetTextI18n")
    private fun getFromTime(){
        val mCurrentTime = Calendar.getInstance()
        val hour = mCurrentTime[Calendar.HOUR_OF_DAY]
        val minute = mCurrentTime[Calendar.MINUTE]
        val mTimePicker: TimePickerDialog
        mTimePicker = TimePickerDialog(
            requireContext(),
            { _, selectedHour, selectedMinute ->
                binding.textTimeFromValue.text = "$selectedHour:$selectedMinute"
                timeFrom = "$selectedHour:$selectedMinute"
                timeFromMiliSecond = calTimeInMilliSecond(
                    selectedYear,
                    selectedMonth,
                    selectedDay,
                    selectedHour,
                    selectedMinute
                )
                Log.i(TAG, "timeFromMiliSecond $timeFromMiliSecond")
            },
            hour,
            minute,
            false
        )
        mTimePicker.setTitle("Select Time")
        mTimePicker.show()
    }
    @SuppressLint("SetTextI18n")
    private fun getToTime(){
        val mCurrentTime = Calendar.getInstance()
        val hour = mCurrentTime[Calendar.HOUR_OF_DAY]
        val minute = mCurrentTime[Calendar.MINUTE]
        val mTimePicker: TimePickerDialog
        mTimePicker = TimePickerDialog(
            requireContext(),
            { _, selectedHour, selectedMinute ->
                binding.textTimeToValue.text = "$selectedHour:$selectedMinute"
                timeTo = "$selectedHour:$selectedMinute"
                timeToMiliSecond = calTimeInMilliSecond(
                    selectedYear,
                    selectedMonth,
                    selectedDay,
                    selectedHour,
                    selectedMinute
                )
                Log.i(TAG, "timeToMiliSecond $timeToMiliSecond")
            },
            hour,
            minute,
            false
        )
        mTimePicker.setTitle("Select Time")
        mTimePicker.show()
    }
    private fun checkData(){
        if(binding.textDateValue.text.isNullOrBlank() ||
            binding.textTimeFromValue.text.isNullOrBlank()||
            binding.textTimeToValue.text.isNullOrBlank()||
            timeFromMiliSecond <= Calendar.getInstance().timeInMillis){
            if (binding.textDateValue.text.isNullOrBlank()) {
                binding.textDateValue.setTextColor(Color.RED)
                binding.textDateValue.text = resources.getString(R.string.date_required)
            }


            if (binding.textTimeFromValue.text.isNullOrBlank()) {
                binding.textTimeFromValue.setTextColor(Color.RED)
                binding.textTimeFromValue.text = resources.getString(R.string.time_required)
            }

            if (binding.textTimeToValue.text.isNullOrBlank()) {
                binding.textTimeToValue.setTextColor(Color.RED)
                binding.textTimeToValue.text = resources.getString(R.string.time_required)
            }
            if (timeFromMiliSecond <= Calendar.getInstance().timeInMillis) {
                binding.textTimeFromValue.setTextColor(Color.RED)
                showLongToast(
                    requireContext(),
                    "Time from must be bigger than current Time"
                )
            }

        }else{
            val coordinate = listOf(53, 37)
            val area = listOf(Area(coordinate, "Point"))
            val condition = listOf(Condition(299, "\$gt", apiEventType))

            val startPoint = Start(timeFromMiliSecond.toInt(), "after")
            val endPoint = End(timeToMiliSecond.toInt(), "after")

            val timePeroid = TimePeriod(endPoint, startPoint)

            repeated?.let {
                viewModel.saveAlarmData(
                    Alarm(
                        eventType = eventType, active = true, timeFrom = timeFrom,
                        timeTo = timeTo, date = dateString, workManagerTag = generateTag(),
                        repeated = repeated
                    ), TriggerRequest(area, condition, timePeroid),
                    it,calDurationTime(),generateTag()
                )
            }
            //generateWorkManager()
            //dismiss()
        }
    }

    private fun calTimeInMilliSecond(
        selectDateYear: Int, selectDateMonth: Int,
        selectDateDay: Int, selectDateTimeHou: Int,
        selectDateTimeMin: Int
    ):Long{
        val calendar1 = Calendar.getInstance()
        calendar1.set(
            selectDateYear,
            selectDateMonth,
            selectDateDay,
            selectDateTimeHou,
            selectDateTimeMin,
            0
        )
        return calendar1.timeInMillis

    }
    private fun generateTag() = "$selectedDay-$selectedMonth-" +
            "$selectedYear-$timeFromMiliSecond-$timeToMiliSecond"

    private fun calDurationTime():Long{
        val calendar = Calendar.getInstance()
        return  timeFromMiliSecond - calendar.timeInMillis
    }

    companion object {

        fun newInstance() = DialogAddAlarm()
        const val TAG = "AddAlarmDialog"
    }
}