package com.wesam.weatherforecast.ui

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import com.wesam.weatherforecast.R
import com.wesam.weatherforecast.utils.getLocaleContext

open class BaseActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.AppTheme)
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        Log.i("BaseActivity","onCreate")

    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(getLocaleContext(newBase))
        Log.i("BaseActivity","attachBaseContext")
    }
}