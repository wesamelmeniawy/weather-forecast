package com.wesam.weatherforecast.ui.alarm


import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.work.WorkManager
import com.wesam.weatherforecast.model.ModelRepository
import com.wesam.weatherforecast.model.data_classes.Alarm
import com.wesam.weatherforecast.utils.Event

class AlarmViewModel(application: Application): AndroidViewModel(application) {

    private val _modelRepository : ModelRepository = ModelRepository(application.applicationContext)
    private val alarmWorkManager: WorkManager =  WorkManager.getInstance(application)

    private  var _addAlarmFab = MutableLiveData<Event<Boolean>>()
    private  var _showDeleteItemDialog = MutableLiveData<Event<Int?>>()
    private  var _notificationActive = MutableLiveData<Event<Boolean?>>()
    private  var _notificationNotActive = MutableLiveData<Event<Boolean?>>()
    private  var _alarmActive = MutableLiveData<Event<Boolean?>>()
    private  var _alarmNotActive = MutableLiveData<Event<Boolean?>>()

    private var allAlarm:LiveData<List<Alarm>>
   // private var activeAlarm:LiveData<List<Alarm>>


    val addAlarmFab:LiveData<Event<Boolean>>
    get() = _addAlarmFab

    val showDeleteItemDialog:LiveData<Event<Int?>>
        get() = _showDeleteItemDialog

    val notificationActive:LiveData<Event<Boolean?>>
        get() = _notificationActive

    val notificationNotActive:LiveData<Event<Boolean?>>
        get() = _notificationNotActive

    val alarmActive:LiveData<Event<Boolean?>>
        get() = _alarmActive

    val alarmNotActive:LiveData<Event<Boolean?>>
        get() = _alarmNotActive

    init {
        allAlarm = _modelRepository.getAllAlarm()
    }

    fun getAllAlarm():LiveData<List<Alarm>>{
        return allAlarm
    }


    fun fabAddAlarmClicked(){
        _addAlarmFab.postValue(Event(true))
    }

    fun deleteItemClicked(id:Int?){
        Log.i(TAG,"ID $id")
        _showDeleteItemDialog.postValue(Event(id))
    }

    fun deleteRow(id: Int){
        _modelRepository.deleteAlarm(id)
    }


    fun changeNotification(active: Boolean?,id: Int?){
        Log.i(TAG,"ID $active")
        if(active == true){
                _modelRepository.updateAlarmActivation(false,id)
            _notificationNotActive.postValue(Event(true))
        }else{
                _modelRepository.updateAlarmActivation(true,id)

            _notificationActive.postValue(Event(true))
        }

    }

    fun changeAlarmStatus(alarmStatus: Boolean?,id: Int?){
        if(alarmStatus == true){
            _modelRepository.updateAlarmStatus(false,id)
            _alarmNotActive.postValue(Event(true))
        }else{
            _modelRepository.updateAlarmStatus(true,id)
            _alarmActive.postValue(Event(true))
        }
    }

    fun cancelWorkManager(tag: String){
       alarmWorkManager.cancelAllWorkByTag(tag)
    }


    companion object{
        const val TAG = "AlarmViewModel"
    }
}