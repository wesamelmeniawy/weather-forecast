package com.wesam.weatherforecast.ui.favoritdetails

import android.app.Application
import android.view.View
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.squareup.moshi.JsonAdapter
import com.wesam.weatherforecast.model.ModelRepository
import com.wesam.weatherforecast.model.data_classes.Weather
import com.wesam.weatherforecast.ui.main.DailyAdapter
import com.wesam.weatherforecast.ui.main.HourlyAdapter
import com.wesam.weatherforecast.utils.*
import com.wesam.weatherforecast.utils.helper.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.*

class FavoriteDetailsViewModel(application: Application): AndroidViewModel(application) {

    private var _textCurrentDate = MutableLiveData<String>()
    private var _humidity = MutableLiveData<String>()
    private var _clouds = MutableLiveData<String>()
    private var _pressure = MutableLiveData<String>()
    private var _windSpeed = MutableLiveData<String>()
    private var _countryName = MutableLiveData<String>()
    private var _temperature = MutableLiveData<String>()
    private var _description = MutableLiveData<String>()
    private var _constantVisibility = MutableLiveData<Int>()
    private var _animationVisibility = MutableLiveData<Int>()

    private lateinit  var _tempUnit:String
    private val _modelRepository : ModelRepository = ModelRepository(application.applicationContext)
    var cityName:String? = ""
    private var _langType: String
    private var _windUnit: String

    val humidity: LiveData<String>
        get() = _humidity

    val clouds: LiveData<String>
        get() = _clouds

    val pressure: LiveData<String>
        get() = _pressure

    val windSpeed: LiveData<String>
        get() = _windSpeed

    val countryName: LiveData<String>
        get() = _countryName

    val temperature: LiveData<String>
        get() = _temperature
    val description: LiveData<String>
        get() = _description

    val constantVisibility: LiveData<Int>
        get() = _constantVisibility

    val textCurrentDate: LiveData<String>
        get() = _textCurrentDate

    val animationVisibility:LiveData<Int>
        get() = _animationVisibility

    private val moshiObject = moshi

     var dailyAdapter: DailyAdapter
     var hourlyAdapter: HourlyAdapter

    private var list: List<String> = ArrayList()


    init {
        _constantVisibility.postValue(View.GONE)
        _animationVisibility.postValue(View.VISIBLE)

        _langType = _modelRepository.getLanguage()
        _windUnit = _modelRepository.getWindUnit()
        dailyAdapter = DailyAdapter(_modelRepository.getTempUnit(),_langType)
        hourlyAdapter = HourlyAdapter(_modelRepository.getTempUnit(),_langType)
    }

    fun getList(weatherString:String){
        convertStringToList(weatherString)
    }


    private fun convertStringToList(weatherString:String){
        val adapterWeather: JsonAdapter<Weather?> = moshiObject.adapter(Weather::class.java)
        val cityWeatherList: Weather? = adapterWeather.fromJson(weatherString)
        cityWeatherList?.latitude?.let { cityWeatherList.longitude?.let { it1 ->
            getWeather(it,
                it1,_langType)
        } }
        setData(cityWeatherList)
    }

    private fun setData(response: Weather?){
        if(response?.current != null){
            _animationVisibility.postValue(View.GONE)
            cityName = response.countryName
            _humidity.postValue("${response.current.humidity} %")
            _clouds.postValue("${response.current.clouds} %")
            setWindSpeed(response.current.wind_speed)
            _pressure.postValue("${response.current.pressure} " +
                    "${setPressure(_modelRepository.getLanguage())} ")
            list = if (_modelRepository.getLanguage() == DEFAULT_LANG_STRING){
                splitDate(dateFormat(response.current.date, Locale.ENGLISH), Locale.ENGLISH)

            }else{
               // splitDate(dateFormat(response.current.date, Locale("ar")), Locale("ar"))
                splitDate(dateFormat(response.current.date, Locale("ar")), Locale.ENGLISH)
            }
            if(list.size > 1){
                _textCurrentDate.postValue("${list[2]} ${list[1]}")
            }
            _description.postValue("${response.current.weather?.get(0)?.description} ")
            _constantVisibility.postValue(View.VISIBLE)
            setTemp(response.current.temperature)
            response.let { hourlyAdapter.loadData(it.hourly,_tempUnit,_langType) }
            response.let { dailyAdapter.loadData(it.daily,_tempUnit,_langType) }
        }else{
            cityName = response?.countryName
        }

    }
   private fun setTemp(temp: Double?){
        _tempUnit = _modelRepository.getTempUnit()
        if(_tempUnit.isBlank()){
            _tempUnit = DEFAULT_TEMP_STRING
        }
        val tempUnitString = calTempUnit(temp,_tempUnit)
        _temperature.postValue(tempUnitString)
    }

    private fun setWindSpeed(windSpeed: Float?){
        _windUnit = _modelRepository.getWindUnit()

        if(_windUnit.isBlank()){
            _windUnit = DEFAULT_WIND_STRING
        }


        if(windSpeed != null){
            val windSpeedUnitString = calWindSpeedUnit(windSpeed,_windUnit,_langType)
            _windSpeed.postValue(windSpeedUnitString)
        }
    }

    private fun getWeather(latitude: Float, longitude:Float,lang:String){
        viewModelScope.launch(Dispatchers.IO) {
            _modelRepository.getWeather(latitude, longitude, lang)
        }
    }

}