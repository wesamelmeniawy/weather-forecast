package com.wesam.weatherforecast.ui.favorite

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.wesam.weatherforecast.model.ModelRepository
import com.wesam.weatherforecast.model.data_classes.Weather
import com.wesam.weatherforecast.utils.Event

class FavoriteViewModel(application: Application): AndroidViewModel(application) {

    private var _navigateToMap = MutableLiveData<Event<Boolean>>()
    private var _navigateToItemDetails = MutableLiveData<Event<Int>>()


    private var allWeather:LiveData<List<Weather>>

    val navigateToMap:LiveData<Event<Boolean>>
    get() = _navigateToMap


    val navigateToItemDetails:LiveData<Event<Int>>
        get() = _navigateToItemDetails

    private val _modelRepository : ModelRepository = ModelRepository(application.applicationContext)

    init {
        allWeather = _modelRepository.getAllWeather()
    }



    fun fabButtonClicked(){
        _navigateToMap.postValue(Event(true))
    }

    fun getAllWeather():LiveData<List<Weather>>{
//        favoriteAdapter.loadData(it)
        return allWeather
    }
    fun getTempUnit(): String{
        return _modelRepository.getTempUnit()
    }

    fun deleteRow(id: Int){
        _modelRepository.deleteFavoriteRow(id)
    }

    fun listItemClicked(id: Int){
        Log.i("Favorite ViewModel",id.toString())
        _navigateToItemDetails.postValue(Event(id))
    }

    fun listItemLongClicked(id: Int){
        Log.i("Favorite ViewModel",id.toString())
       // _navigateToItemDetails.postValue(Event(id))
    }
    fun setCurrentPosition(){
        _modelRepository.setPositionedCountryName("")
    }
}