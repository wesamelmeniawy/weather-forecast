package com.wesam.weatherforecast.ui.main

import android.annotation.SuppressLint
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.wesam.weatherforecast.databinding.DailyRowItemBinding
import com.wesam.weatherforecast.model.data_classes.Daily
import com.wesam.weatherforecast.utils.*
import com.wesam.weatherforecast.utils.helper.calTempUnit
import com.wesam.weatherforecast.utils.helper.dateFormat
import com.wesam.weatherforecast.utils.helper.splitDate
import java.util.*
import kotlin.collections.ArrayList

class DailyAdapter(private var tempUnit: String,private var langType: String):
    RecyclerView.Adapter<DailyAdapter.ViewHolder>(){

    private var dailyList: List<Daily?> = ArrayList()
    private var list: List<String> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val viewBinding = DailyRowItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent, false
        )
        return ViewHolder(viewBinding)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.textMaxTemp.text = calTempUnit(
            dailyList[position]?.temperature?.maxTemperature, tempUnit
        )

        holder.binding.textMinTemp.text = calTempUnit(
            dailyList[position]?.temperature?.minTemperature, tempUnit
        )

        list = when (langType) {
            DEFAULT_LANG_STRING -> {
                splitDate(dateFormat(dailyList[position]?.date, Locale.ENGLISH), Locale.ENGLISH)
            }
            ARABIC_LANG_STRING -> {
                splitDate(dateFormat(dailyList[position]?.date, Locale("ar")), Locale.ENGLISH)
            }
            else -> {
                splitDate(dateFormat(dailyList[position]?.date, Locale.ENGLISH), Locale.ENGLISH)
            }
        }

         Log.i("DailyAdapter",list.size.toString())
        if(list.size > 1){
            holder.binding.textDate.text = list[2] + " "+ list[1]
            holder.binding.textDay.text = list[0]
        }

        val url = IMG_URL+ dailyList[position]?.weather?.get(0)!!.icon+".png"
        Log.i("onBindViewHolder",url)
        Glide.with(holder.binding.imgWeather.context)
            .load(url)
            .into(holder.binding.imgWeather)
    }
    inner class ViewHolder(var binding: DailyRowItemBinding) :
        RecyclerView.ViewHolder(binding.root)


    override fun getItemCount(): Int {
        return dailyList.size
    }

    fun loadData(dailyList: List<Daily?>, tempUnit: String, langType: String) {
        this.langType = langType
        this.tempUnit = tempUnit
        this.dailyList = dailyList

        notifyDataSetChanged()
    }

}