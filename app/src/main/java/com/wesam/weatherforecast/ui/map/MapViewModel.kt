package com.wesam.weatherforecast.ui.map

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.wesam.weatherforecast.model.ModelRepository
import com.wesam.weatherforecast.model.data_classes.Weather
import com.wesam.weatherforecast.utils.ARABIC_LANG_STRING
import com.wesam.weatherforecast.utils.DEFAULT_LANG_STRING
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MapViewModel(application: Application): AndroidViewModel(application) {
    private val _modelRepository : ModelRepository = ModelRepository(application.applicationContext)

//    fun getCountry(latitude: Double, longitude: Double):String{
//       val country =  _modelRepository.getCountryName(latitude, longitude)
//        Log.i("MapViewMode","Country $country")
//        return if(country == null){
//            Log.i("MapViewMode","Country null null")
//            "no city found"
//        }else{
//            country
//        }
//    }
    fun setLatLongToPref(latitude: Double, longitude: Double){
        Log.i("MapViewMode","$latitude $longitude ")
        Log.i("MapViewMode","${latitude.toFloat()} ${longitude.toFloat()} ")
        _modelRepository.setPositionLatitude(latitude.toFloat())
        _modelRepository.setPositionLongitude(longitude.toFloat())
        Log.i("MapViewModel","latlong ${_modelRepository.getPositionLatitude()} ${_modelRepository.getPositionLongitude()} ")

    }
    fun insertCountry(weather: Weather){
        viewModelScope.launch(Dispatchers.IO) {
            _modelRepository.insertCountry(weather)
        }
        //_modelRepository.insertCountry(weather)
    }

    fun setCityName(countyName: String){
        _modelRepository.setPositionedCountryName(countyName)
    }
    fun getCityName():String{
        return _modelRepository.getPositionedCountryName()
    }
    fun getSelectedWeather(latitude: Float, longitude: Float){
        Log.i("getSelectedWeather","Lang ${_modelRepository.getLanguage()}")
        CoroutineScope(Dispatchers.IO).launch {
            when (_modelRepository.getLanguage()) {
                ""->_modelRepository.getWeather(latitude, longitude, "en")
                DEFAULT_LANG_STRING -> _modelRepository.getWeather(latitude, longitude, "en")
                ARABIC_LANG_STRING -> _modelRepository.getWeather(latitude, longitude, "ar")

            }
            //Log.i("favoriteViewModel","${result.current}")
        }
    }
    fun setLocationOption(locationOption: String){
        _modelRepository.setLocationOption(locationOption)
    }

    fun getCityName(cityName:String):String{
        val countryList =  _modelRepository.getAllCountry()
        var countryName = " city not found"
        for(i in countryList){
            if (cityName == i){
                countryName = i
                break
            }
        }
        return countryName
    }
}