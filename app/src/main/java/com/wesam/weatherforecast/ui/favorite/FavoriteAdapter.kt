package com.wesam.weatherforecast.ui.favorite

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.wesam.weatherforecast.databinding.FavoriteRowItemBinding
import com.wesam.weatherforecast.model.data_classes.Weather
import com.wesam.weatherforecast.utils.helper.calTempUnit

class FavoriteAdapter(private val listener:FavoriteViewModel, private var tempUnit:String):
    RecyclerView.Adapter<FavoriteAdapter.ViewHolder>(){

    private var weatherList: List<Weather?> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val viewBinding = FavoriteRowItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent, false
        )
        return ViewHolder(viewBinding)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.binding.textCityName.text = weatherList[position]?.countryName
        //holder.binding.textTemp.text = weatherList[position]?
        // .current?.temperature?.toInt().toString()

        if(calTempUnit(weatherList[position]?.
            current?.temperature,tempUnit) != "null" && calTempUnit(weatherList[position]?.
            current?.temperature,tempUnit).isNotBlank()){
            holder.binding.textTemp.visibility = View.VISIBLE
            holder.binding.textTempType.visibility = View.VISIBLE
            holder.binding.textTemp.text = calTempUnit(weatherList[position]?.
            current?.temperature,tempUnit)
       }

    }
    inner class ViewHolder(var binding: FavoriteRowItemBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener,
        View.OnLongClickListener{
        init {
            binding.cardItem.setOnClickListener(this)
            binding.cardItem.setOnLongClickListener(this)
        }
        override fun onClick(v: View?) {
            val position = adapterPosition
            listener.listItemClicked(position)
        }

        override fun onLongClick(v: View?):Boolean {
            val position = adapterPosition
            listener.listItemLongClicked(position)
            return true
        }

    }


    override fun getItemCount(): Int {
        return weatherList.size
    }

    fun loadData(weatherList: List<Weather?>, tempUnit: String) {
        this.tempUnit = tempUnit
        this.weatherList = weatherList
        notifyDataSetChanged()
    }
}