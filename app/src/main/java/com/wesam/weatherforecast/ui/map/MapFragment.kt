package com.wesam.weatherforecast.ui.map

import android.content.Context
import android.location.Address
import android.location.Geocoder
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.wesam.weatherforecast.R
import com.wesam.weatherforecast.model.data_classes.Weather
import com.wesam.weatherforecast.utils.KEY_CHANGE_MAP_LOCATION_BROADCAST
import com.wesam.weatherforecast.utils.KEY_POSITION
import com.wesam.weatherforecast.utils.MAP_LOCATION_STRING
import com.wesam.weatherforecast.utils.helper.networkConnection
import com.wesam.weatherforecast.utils.helper.sendBroadCast
import com.wesam.weatherforecast.utils.helper.showLongToast
import com.wesam.weatherforecast.utils.helper.showShortMessage
import java.io.IOException
import java.util.*


class MapFragment : FragmentActivity(), OnMapReadyCallback {

    private lateinit var mMap:GoogleMap
    private lateinit var viewModel: MapViewModel
    private lateinit var geoCoder: Geocoder
    //private var prevMarker:Marker? =  null
    private lateinit var conMgr: ConnectivityManager
    private  var position:Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.AppTheme)
        setContentView(R.layout.fragment_map)

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as? SupportMapFragment
        mapFragment?.getMapAsync(this)

        init()
    }

    private fun init(){
        viewModel = ViewModelProvider(this,ViewModelProvider
            .AndroidViewModelFactory(application)).get(MapViewModel::class.java)

        geoCoder = Geocoder(this, Locale.getDefault())

        conMgr = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        position = intent.getBooleanExtra(KEY_POSITION,false)
        Log.i(TAG,"position $position")
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        googleMap?.let { mMap= it }
        mMap.uiSettings.isZoomControlsEnabled = true

        val zaragoza = LatLng(26.77619851601672, 29.65819791593859)
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(zaragoza,7f))

        mMap.setOnMapClickListener {
            mMap.clear()
            Log.i(TAG, "LatLong ${it.latitude} ${it.longitude}")

            mMap.addMarker(
                MarkerOptions()
                    .position(it)
            )

            if(networkConnection(conMgr,this)){
                getPositionInfo(it)
            }else{
                showShortMessage(this,"network connection is required")
            }
        }

    }

    private fun getPositionInfo(latLong: LatLng){
        try {
            val addresses: List<Address> = geoCoder.getFromLocation(latLong.latitude,
                latLong.longitude, 1)

            val cityName = addresses[0].locality

            if(cityName != null) {
                val cityFromDB = viewModel.getCityName(cityName)
                Log.i(TAG, "cityDB $cityFromDB")
                Log.i(TAG, "city $cityName")
                if (cityFromDB != cityName) {
                    viewModel.insertCountry(
                        Weather(
                            countryName = cityName,
                            latitude = latLong.latitude.toFloat(),
                            longitude = latLong.longitude.toFloat()
                        )
                    )
                    viewModel.getSelectedWeather(latLong.latitude.toFloat(),
                        latLong.longitude.toFloat())

                    if(position){
                        viewModel.setLatLongToPref(latLong.latitude, latLong.longitude)
                        viewModel.setCityName(cityName)
                        viewModel.setLocationOption(MAP_LOCATION_STRING)
                        Log.i(TAG,"getCityName ${viewModel.getCityName()}")
                        sendBroadCast(this, KEY_CHANGE_MAP_LOCATION_BROADCAST)
                    }
                    showShortMessage(this,"position added successfully")
                    finish()
                }else{
                    showShortMessage(this,"this position already exist")
                }
            }else{
                showLongToast(this,"make sure you choose correct position")
            }
        }catch (e:IOException){
            Log.e(TAG,"IOException ${e.localizedMessage}")
        }

    }
    companion object{
        const val TAG = "MapFragment"
    }
}