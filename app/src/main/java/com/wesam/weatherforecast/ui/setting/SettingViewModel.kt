package com.wesam.weatherforecast.ui.setting

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.wesam.weatherforecast.model.ModelRepository
import com.wesam.weatherforecast.utils.*

class SettingViewModel(application: Application): AndroidViewModel(application) {

    private var _modelRepository : ModelRepository = ModelRepository(application.applicationContext)

    private var _language: String
    private var _locationOption: String
    private var _tempUnit: String
    private var _windUnit: String

    private var _languageString = MutableLiveData<String>()
    private var _locationOptionString = MutableLiveData<String>()
    private var _tempString = MutableLiveData<String>()
    private var _windString = MutableLiveData<String>()


    private val _languageClicked= MutableLiveData<Event<String>>()
    private val _locationOptionClicked= MutableLiveData<Event<String>>()
    private val _tempUnitClicked= MutableLiveData<Event<String>>()
    private val _windUnitClicked= MutableLiveData<Event<String>>()

    private val _navigateToAlarmActivity= MutableLiveData<Event<Boolean>>()


    val language: LiveData<String>
        get() = _languageString

    val locationOption: LiveData<String>
        get() = _locationOptionString

    val tempUnit: LiveData<String>
        get() = _tempString

    val windUnit: LiveData<String>
        get() = _windString

    val tempUnitClicked: LiveData<Event<String>>
        get() = _tempUnitClicked
    val languageClicked: LiveData<Event<String>>
        get() = _languageClicked
    val locationClicked: LiveData<Event<String>>
        get() = _locationOptionClicked
    val windUnitClicked: LiveData<Event<String>>
        get() = _windUnitClicked

    val navigateToAlarm: LiveData<Event<Boolean>>
        get() = _navigateToAlarmActivity


    init {
        _language = _modelRepository.getLanguage()
        if(_language == ""){
            _language = DEFAULT_LANG_STRING
            _modelRepository.setLanguage(_language)
        }
        _locationOption= _modelRepository.getLocationOption()
        _tempUnit= _modelRepository.getTempUnit()
        _windUnit = _modelRepository.getWindUnit()
        _languageString.postValue(_language)
    }

    fun tempClicked(){
        if(_tempUnit.isBlank()){
            _tempUnit = DEFAULT_TEMP_STRING
            _modelRepository.setTempUnit(_tempUnit)
        }
        _tempUnitClicked.postValue(Event(_tempUnit))
    }

    fun windClicked(){
        if(_windUnit.isBlank()){
            _windUnit = DEFAULT_WIND_STRING
            _modelRepository.setWindUnit(_windUnit)
        }
        _windUnitClicked.postValue(Event(_windUnit))
    }
    fun languageClicked(){
        if(_language.isBlank()){
            _language = DEFAULT_LANG_STRING
            _modelRepository.setLanguage(_language)
        }
         //languageString.postValue(_language)
        _languageClicked.postValue(Event(_language))
    }
    fun locationOptionClicked(){
        if(_locationOption.isBlank()){
            _locationOption = DEFAULT_LOCATION_STRING
            _modelRepository.setLocationOption(_locationOption)
        }
        _locationOptionClicked.postValue(Event(_locationOption))
    }

    fun langString(){
        _language = _modelRepository.getLanguage()
        if(_language.isBlank()){
            _language = DEFAULT_LANG_STRING
            _modelRepository.setLanguage(_language)
        }
        if(_language == ARABIC_LANG_STRING){
            when(_language){
                DEFAULT_LANG_STRING-> _languageString.postValue(ARABIC_DEFAULT_LANG_STRING)
                ARABIC_LANG_STRING-> _languageString.postValue(ARABIC_ARABIC_LANG_STRING)

            }
        }else{
            _languageString.postValue(_language)
        }

    }

    fun windString(){
        _windUnit = _modelRepository.getWindUnit()
        if(_windUnit.isBlank()){
            _windUnit = DEFAULT_WIND_STRING
            _modelRepository.setWindUnit(_windUnit)
        }
        if(_language == ARABIC_LANG_STRING){
            when(_windUnit){
                DEFAULT_WIND_STRING-> _windString.postValue(ARABIC_DEFAULT_WIND_STRING)
                HOUR_WIND_STRING-> _windString.postValue(ARABIC_HOUR_WIND_STRING)

            }
        }else{
            _windString.postValue(_windUnit)
        }

    }

    fun tempString(){
        _tempUnit = _modelRepository.getTempUnit()
        if(_tempUnit.isBlank()){
            _tempUnit = DEFAULT_TEMP_STRING
            _modelRepository.setTempUnit(_tempUnit)
        }
        if(_language == ARABIC_LANG_STRING){
            when(_tempUnit){
                DEFAULT_TEMP_STRING-> _tempString.postValue(ARABIC_DEFAULT_TEMP_STRING)
                FAHRENHEIT_TEMP_STRING-> _tempString.postValue(ARABIC_FAHRENHEIT_TEMP_STRING)
                CELSIUS_TEMP_STRING-> _tempString.postValue(ARABIC_CELSIUS_TEMP_STRING)

            }
        }else{
            _tempString.postValue(_tempUnit)
        }
    }

    fun locationString(){
        _locationOption = _modelRepository.getLocationOption()
        if(_locationOption.isBlank()){
            _locationOption = DEFAULT_LOCATION_STRING
            _modelRepository.setLocationOption(_locationOption)
        }
        if(_language == ARABIC_LANG_STRING){
            when(_locationOption){
                DEFAULT_LOCATION_STRING-> _locationOptionString.postValue(
                    ARABIC_DEFAULT_LOCATION_STRING)
                MAP_LOCATION_STRING-> _locationOptionString.postValue(ARABIC_MAP_LOCATION_STRING)
            }
        }else{
            _locationOptionString.postValue(_locationOption)
        }

    }

    fun navigateToAlarmActivity(){
        _navigateToAlarmActivity.postValue(Event(true))
    }


}