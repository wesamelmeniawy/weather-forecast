package com.wesam.weatherforecast.utils

import android.graphics.Typeface
import android.text.Spannable
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.TextUtils
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.util.Log
import android.view.View
import android.widget.*
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.gif.GifDrawable
import com.bumptech.glide.request.RequestListener
import com.wesam.weatherforecast.R
import com.wesam.weatherforecast.utils.extension.getParentActivity
import kotlin.time.minutes


/**
 * Set [TextView] text.
 *
 * @param titleDetails: [MutableLiveData] of List<[Any]>
 *     List[0] -> Title string id (Integer).
 *     List[1] -> Title color id (Integer).
 *     List[2] -> Is title bold (Boolean).
 *     List[3] -> Details (String).
 *     List[4] -> Details color id (Integer).
 *     List[5] -> Is details bold (Boolean).
 */
@BindingAdapter(
    "mutableText",
    "mutableTextId",
    "mutableTitleDetails",
    "mutableSpannableStringBuilder",
    requireAll = false
)
fun setMutableText(
    view: TextView,
    @Nullable text: LiveData<String>?,
    @Nullable textId: LiveData<Int>?,
    @Nullable titleDetails: LiveData<List<Any>>?,
    @Nullable spannableStringBuilder: LiveData<SpannableStringBuilder>?
) {
    val parentActivity: AppCompatActivity? = view.getParentActivity()

    if (parentActivity != null) {
        text?.let { text.observe(parentActivity, Observer { value -> view.text = value ?: "" }) }

        textId?.let {
            textId.observe(
                parentActivity,
                Observer { value -> value?.let { view.setText(value) } })
        }

        titleDetails?.let {
            titleDetails.observe(parentActivity, Observer { value ->
                Log.i("BindingAdapter","Setting title details data with data: $value")

                // Title.
                val spannableTitle = SpannableString("${view.context.getString(value[0] as Int)}: ")

                spannableTitle.setSpan(
                    ForegroundColorSpan(ContextCompat.getColor(view.context, value[1] as Int)),
                    0,
                    spannableTitle.length,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                )

                if (value[2] as Boolean) {
                    spannableTitle.setSpan(
                        StyleSpan(Typeface.BOLD),
                        0,
                        spannableTitle.length,
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                    )
                }

                // Details.
                val spannableDetails = SpannableString(value[3] as String)

                spannableDetails.setSpan(
                    ForegroundColorSpan(ContextCompat.getColor(view.context, value[4] as Int)),
                    0,
                    spannableDetails.length,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                )

                if (value[5] as Boolean) {
                    spannableDetails.setSpan(
                        StyleSpan(Typeface.BOLD),
                        0,
                        spannableDetails.length,
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                    )
                }

                // Set text to text view.
                view.text = TextUtils.concat(spannableTitle, spannableDetails)
            })
        }

        spannableStringBuilder?.let {
            spannableStringBuilder.observe(
                parentActivity,
                Observer { value -> view.setText(value, TextView.BufferType.SPANNABLE) })
        }
    }
}

/**
 * Set [Button] text.
 */
@BindingAdapter("mutableText", "mutableTextId", requireAll = false)
fun setMutableText(view: Button, @Nullable text: LiveData<String>?, @Nullable textId: LiveData<Int>?) {
    val parentActivity: AppCompatActivity? = view.getParentActivity()

    if (parentActivity != null) {
        text?.let { text.observe(parentActivity, Observer { value -> view.text = value ?: "" }) }

        textId?.let {
            textId.observe(
                parentActivity,
                Observer { value -> value?.let { view.setText(value) } })
        }
    }
}

/**
 * Set [AppCompatEditText] text.
 */
@BindingAdapter("mutableText", "mutableTextId", requireAll = false)
fun setMutableText(
    view: AppCompatEditText,
    @Nullable text: LiveData<String>?,
    @Nullable textId: LiveData<Int>?
) {
    val parentActivity: AppCompatActivity? = view.getParentActivity()

    if (parentActivity != null) {
        text?.let { text.observe(parentActivity, Observer { value -> view.setText(value) }) }

        textId?.let {
            textId.observe(
                parentActivity,
                Observer { value -> value?.let { view.setText(value) } })
        }
    }
}


/**
 * Set [RecyclerView] adapter.
 */
@BindingAdapter("adapter")
fun setAdapter(view: RecyclerView, @Nullable adapter: RecyclerView.Adapter<*>?) {
    adapter?.let { view.adapter = adapter }
}


/**
 * Set [View] visibility.
 */
@BindingAdapter("mutableVisibility")
fun setMutableVisibility(view: View, @Nullable visibility: LiveData<Int>?) {
    val parentActivity: AppCompatActivity? = view.getParentActivity()

    if (parentActivity != null && visibility != null) {
        visibility.observe(
            parentActivity,
            Observer { value -> view.visibility = value ?: View.VISIBLE })
    }
}
/**
 * Set [CompoundButton] properties.
 */
@BindingAdapter("mutableCheck", requireAll = false)
fun bindingAdapterCompoundButton(view: CompoundButton, @Nullable checked: LiveData<Boolean?>?) {
    view.getParentActivity()?.let { parentActivity ->
        checked?.let { checked ->
            checked.observe(
                parentActivity,
                { value -> value?.let { view.isChecked = it } })
        }
    }
}




