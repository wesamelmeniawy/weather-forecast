package com.wesam.weatherforecast.utils

import android.content.Context
import android.content.res.Configuration
import android.util.Log
import com.wesam.weatherforecast.model.ModelRepository
import java.util.*


fun getLocaleContext(context: Context?): Context? {
    context?.let {
        val locale = Locale(getLocale(context))
        //Locale.setDefault(locale)
        val res = context.resources
        val config = Configuration(res.configuration)
        config.setLocale(locale)
        //config.setLayoutDirection(locale)
        return context.createConfigurationContext(config)
    }

    return context
}
fun getLocale(context: Context): String {
    val modelRepository = ModelRepository(context = context)
    var currentLang  = modelRepository.getLanguage()
    when(currentLang){
        DEFAULT_LANG_STRING-> currentLang = "en"
        ARABIC_LANG_STRING-> currentLang = "ar"
    }
    Log.i("Helper",currentLang)
    return  currentLang
}