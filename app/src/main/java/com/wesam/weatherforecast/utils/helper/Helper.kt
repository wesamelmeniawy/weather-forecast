package com.wesam.weatherforecast.utils.helper

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentActivity
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.wesam.weatherforecast.utils.*
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


@SuppressLint("SimpleDateFormat")
fun dateFormat(milliSeconds: Long?, lang:Locale): String {
    milliSeconds?.let {
        val time = it * 1000.toLong()
        val date = Date(time)
        val format = SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy a", lang)
        return format.format(date)
    }
//    Log.i("Main",format.format(date?.let { it }))
    return ""
}

@SuppressLint("SimpleDateFormat")
fun splitDate(dateFormat: String?, lang:Locale): List<String> {
    Log.i("splitDate","dateFormat $dateFormat")
    if (dateFormat.isNullOrEmpty()) {
        return listOf()
    }else {
        val list = mutableListOf<String>()
        val date = dateFormat.split(" ")
        return if (date.size > 2) {
            val timeString = date[3]
            val format = SimpleDateFormat("hh:mm:ss",lang)
            val format2 = SimpleDateFormat("hh:mm a", lang)
            val timeParse = format.parse(timeString)
            val time = format2.format(timeParse)
            list.add(date[0])
            list.add(date[1])
            list.add(date[2])
            list.add(time)
            list
        }else{
            listOf()
        }

    }
}

fun navigateActivity(context: Context, activity: AppCompatActivity) {
    context.startActivity(Intent(context, activity::class.java))
}

fun navigateActivity(currentActivity: AppCompatActivity, activity: AppCompatActivity) {
    currentActivity.startActivity(Intent(currentActivity, activity::class.java))
}




fun showLongToast(context: Context, message: String){
    Toast.makeText(context, message, Toast.LENGTH_LONG).show()
}
fun showShortMessage(context: Context, message: String){
    Toast.makeText(context,message,Toast.LENGTH_LONG).show()
}

fun calTempUnit(temp: Double?, tempUnit: String): String{
    var tempUnitString = " "
        when (tempUnit) {
            "" -> tempUnitString = temp?.toInt().toString()
            DEFAULT_TEMP_STRING -> tempUnitString = temp?.toInt().toString()
            CELSIUS_TEMP_STRING -> {
                temp?.let {
                    tempUnitString = (it - 273.15).toInt().toString()
                }
            }
            FAHRENHEIT_TEMP_STRING -> {
                temp?.let {
                    tempUnitString = ((it * 9 / 5) - 459.67).toInt().toString()
                }
            }
    }
    return tempUnitString
}

fun calWindSpeedUnit(windSpeed: Float?, windUnit: String, langType: String): String{
    var windUnitString = " "
    when (windUnit) {
        "" -> {
            windUnitString = if (langType == ARABIC_LANG_STRING) {
                windSpeed?.toInt().toString() + " " + ARABIC_DEFAULT_WIND_STRING
            } else {
                windSpeed?.toInt().toString() + " " + DEFAULT_WIND_STRING
            }
        }
        DEFAULT_WIND_STRING -> {
            windUnitString = if (langType == ARABIC_LANG_STRING) {
                windSpeed?.toInt().toString() + " " + ARABIC_DEFAULT_WIND_STRING
            } else {
                windSpeed?.toInt().toString() + " " + DEFAULT_WIND_STRING
            }
        }
        HOUR_WIND_STRING -> {
            windSpeed?.let {
                windUnitString = if(langType == ARABIC_LANG_STRING){
                    (it * 2.23694).toInt().toString() +" "+ ARABIC_HOUR_WIND_STRING
                }else{
                    (it * 2.23694).toInt().toString() +" "+HOUR_WIND_STRING
                }
            }
        }
    }
    return windUnitString
}
 fun setPressure(lang: String): String{
    var pressureUnitString = " "
    when(lang){
        ""->{pressureUnitString = PRESSURE_ENGLISH}
        DEFAULT_LANG_STRING->{pressureUnitString = PRESSURE_ENGLISH_UNIT}
        ARABIC_LANG_STRING->{pressureUnitString = PRESSURE_ARABIC_UNIT}
    }
    return pressureUnitString
}
fun navigateActivityMap(
    context: Context,
    activity: FragmentActivity,
    positionValue: Boolean
) {
    val intent = Intent(context, activity::class.java)
    intent.apply {
        putExtra(KEY_POSITION, positionValue)
    }
    context.startActivity(intent)
}
 fun sendBroadCast(context: Context, key:String){
    val intent = Intent(key)
    intent.putExtra("message", "sendBroadCast")
    LocalBroadcastManager.getInstance(context).sendBroadcast(intent)
}

@SuppressLint("SimpleDateFormat")
 fun getDate(context: Context): String{
    var date: Date? = null
    var dateString =""
    val onDateSetListener =
        DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
            val months = month + 1
            try {
                date = SimpleDateFormat("dd/MM/yyyy").parse("$dayOfMonth/$months/$year")
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            date?.let {
                dateString = SimpleDateFormat("dd / MM / yyyy").format(it)
            }
        }
    val calendar = Calendar.getInstance(TimeZone.getDefault())

    val dialog = DatePickerDialog(
        context, onDateSetListener,
        calendar[Calendar.YEAR], calendar[Calendar.MONTH],
        calendar[Calendar.DAY_OF_MONTH]
    )
    dialog.setTitle("Please select date.")
    dialog.datePicker.minDate = (System.currentTimeMillis() - 1000)
    dialog.show()
    return dateString
}
@SuppressLint("SetTextI18n")
 fun getTime(context: Context): String{
    var time:String
    val mCurrentTime = Calendar.getInstance()
    val hour = mCurrentTime[Calendar.HOUR_OF_DAY]
    val minute = mCurrentTime[Calendar.MINUTE]
    time = ""
    val mTimePicker: TimePickerDialog
    mTimePicker = TimePickerDialog(
        context,
        { _, selectedHour, selectedMinute -> time = "$selectedHour:$selectedMinute" },
        hour,
        minute,
        false)
    mTimePicker.setTitle("Select Time")
    mTimePicker.show()
    return time
}


