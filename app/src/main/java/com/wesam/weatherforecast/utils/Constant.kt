package com.wesam.weatherforecast.utils

import com.squareup.moshi.Moshi

const val BASE_URL ="https://api.openweathermap.org/data/"
const val IMG_URL = "http://openweathermap.org/img/wn/"
 val moshi =Moshi.Builder().build()

const val DEFAULT_TEMP_STRING = "kelvin"
const val DEFAULT_LANG_STRING = "english"
const val DEFAULT_LOCATION_STRING = "GPS"
const val DEFAULT_WIND_STRING = "meter/sec"

const val CELSIUS_TEMP_STRING = "celsius"
const val ARABIC_LANG_STRING = "arabic"
const val MAP_LOCATION_STRING = "map"
const val HOUR_WIND_STRING = "miles/hour"
const val PRESSURE_ENGLISH_UNIT = "hPa"

const val FAHRENHEIT_TEMP_STRING = "fahrenheit"



const val ARABIC_DEFAULT_TEMP_STRING = "كلفن"
const val ARABIC_CELSIUS_TEMP_STRING = "درجة مئوية"
const val ARABIC_FAHRENHEIT_TEMP_STRING = "فهرنهايت"

const val ARABIC_DEFAULT_LANG_STRING = "الإنجليزية"
const val ARABIC_ARABIC_LANG_STRING = "اللغه العربيه"

const val ARABIC_DEFAULT_LOCATION_STRING = "نظام تحديد المواقع"
const val ARABIC_MAP_LOCATION_STRING = "خريطة"

const val ARABIC_DEFAULT_WIND_STRING = "متر / ثانية"
const val ARABIC_HOUR_WIND_STRING = "ميل / ساعة"

const val PRESSURE_ARABIC_UNIT = "هيكتوباسكال"

const val CURRENT_LANGUAGE = "CURRENT_LANGUAGE"
const val DELETE_ITEM_CANCEL = "DELETE_ITEM_CANCEL"



const val KEY_POSITION = "POSITION"

const val KEY_DETAILS_STRING = "KEY_DETAILS_STRING"
const val KEY_TEMP_UNIT_BROADCAST = "KEY_TEMP_UNIT_BROADCAST"
const val KEY_WIND_UNIT_BROADCAST = "KEY_WIND_UNIT_BROADCAST"
const val KEY_CURRENT_POSITION_FAVORITE_BROADCAST = "KEY_CURRENT_POSITION_BROADCAST"
const val KEY_CHANGE_LOCATION_BROADCAST = "KEY_CHANGE_LOCATION_BROADCAST"
const val KEY_CHANGE_MAP_LOCATION_BROADCAST = "KEY_CHANGE_MAP_LOCATION_BROADCAST"

const val ALARM_ID = "ALARM_ID"

const val HUMIDITY_ENGLISH = "Humidity"
const val PRESSURE_ENGLISH = "Pressure"
const val TEMPERATURE_ENGLISH = "Temperature"
const val WIND_SPEED_ENGLISH = "Wind Speed"
const val WIND_DIRECTION_ENGLISH = "Wind Direction"
const val CLOUDS_ENGLISH = "Clouds"

const val HUMIDITY_ARABIC= "الرطوبه"
const val PRESSURE_ARABIC = "الضغط"
const val TEMPERATURE_ARABIC = "درجة الحراره"
const val WIND_SPEED_ARABIC = "سرعة الرياح"
const val WIND_DIRECTION_ARABIC = "اتجاه الرياح"
const val CLOUDS_ARABIC = "الغيوم"

