package com.wesam.weatherforecast.utils.helper

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.provider.Settings
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.wesam.weatherforecast.R
import com.wesam.weatherforecast.ui.main.MainActivity
import com.wesam.weatherforecast.ui.map.MapFragment
import kotlinx.coroutines.runBlocking

fun showAlertPermissionRequired(activity: MainActivity, setting: String): Boolean {

    val dialogBuilder = AlertDialog.Builder(activity)
    val inflater = activity.layoutInflater
    val dialogView = inflater.inflate(R.layout.permission_dialog, null)
    dialogBuilder.setView(dialogView)

    val cancelButton = dialogView.findViewById(R.id.text_cancel) as TextView
    val enableButton = dialogView.findViewById(R.id.text_enable) as TextView


    val alertDialog = dialogBuilder.create()

    cancelButton.setOnClickListener { alertDialog.dismiss() }
    enableButton.setOnClickListener {
            activity.startActivity(Intent(setting))
            alertDialog.dismiss()
    }

    alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    alertDialog.show()
    return true
}

fun openLocationSettingIntent(activity: AppCompatActivity) {
    val dialogBuilder = AlertDialog.Builder(activity)
    val inflater = activity.layoutInflater
    val dialogView = inflater.inflate(R.layout.permission_dialog, null)
    dialogBuilder.setView(dialogView)

    val cancelButton = dialogView.findViewById(R.id.text_cancel) as Button
    val enableButton = dialogView.findViewById(R.id.text_enable) as Button
    val message = dialogView.findViewById(R.id.text_details) as TextView
    message.text = activity.getString(R.string.location_permission)


    val alertDialog = dialogBuilder.create()

    cancelButton.setOnClickListener { alertDialog.dismiss() }
    enableButton.setOnClickListener {
        alertDialog.dismiss()
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        intent.apply {
            val uri = Uri.fromParts("package", activity.packageName, null)
            data = uri
        }
        activity.startActivity(intent)
    }
    alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    alertDialog.show()

}
fun openLocationMap(activity: AppCompatActivity) {
    val dialogBuilder = AlertDialog.Builder(activity)
    val inflater = activity.layoutInflater
    val dialogView = inflater.inflate(R.layout.permission_dialog, null)
    dialogBuilder.setView(dialogView)

    val cancelButton = dialogView.findViewById(R.id.text_cancel) as Button
    val enableButton = dialogView.findViewById(R.id.text_enable) as Button
    val message = dialogView.findViewById(R.id.text_details) as TextView
    message.text = activity.getString(R.string.need_position_map)
    enableButton.text = activity.resources.getString(R.string.open)


    val alertDialog = dialogBuilder.create()

    cancelButton.setOnClickListener { alertDialog.dismiss() }
    enableButton.setOnClickListener {
        alertDialog.dismiss()
        navigateActivityMap(activity, MapFragment(),true)
    }
    alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    alertDialog.show()

}
